#!/usr/bin/env python

import argparse
import csv
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET



    
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("validationOutput", type=str,
                        help="""An output file that indicates which submissions
                                have passed validation.  This must be a 
                                delimited file with columns named Submission
                                and Status""")
    parser.add_argument('metadataDirOfDirs', type=str,
                        help="""Directory of metadata directories, each of
                                which contain a UUID-named directory that is
                                referenced in the validationOutput file""")
    parser.add_argument('gsc', type=str,
                        help="Name of the GSC for this metadata")
    parser.add_argument('-s', '--subset', type=str, default=None,
                        help="""If given, this specifies a file thta indicates
                             the subset of submissions to report on.
                             This 'subset' is expected to be a 
                             whitespace-delimited file in which the filename
                             is the first token on the line.  If a submission
                             has a filename that matches with a filename on 
                             this list, then it is reported.  Else, it is 
                             ignored.""")
    parser.add_argument('-d', '--debug', type=str, default=False,
                        help="Display debugging info?")
    args = parser.parse_args()

    if args.subset is not None:
        subsetList = readSubset(args.subset)
    print "Submission\tRunAccession\tFilename\tmd5\tSampleBarcode\tGSC\tValidationStatus"
    validationResults = csv.DictReader(open(args.validationOutput), 
                                       delimiter="\t")
    for row in validationResults:
        submission = row["Submission"]
        validationStatus = row["Status"]
        (filename, md5sum) = getFilenameAndMd5sum(args.metadataDirOfDirs,
                                                  submission)
        runAccession = getRunAccession(args.metadataDirOfDirs, submission)
        sampleBarcode = getSampleBarcode(args.metadataDirOfDirs, submission)
        if args.subset is None:
            print "%s\t%s\t%s\t%s\t%s\t%s\t%s" % (submission, runAccession, 
                                                  filename, md5sum, 
                                                  sampleBarcode, 
                                                  args.gsc, 
                                                  validationStatus)
        elif subsetList.has_key(filename):
            print "%s\t%s\t%s\t%s\t%s\t%s\t%s" % (submission, runAccession,
                                                  filename, md5sum, 
                                                  sampleBarcode, 
                                                  args.gsc, 
                                                  validationStatus)



def readSubset(subsetFilename):
    """Read a file that lists the subset of submissions (by BAM filename)
    on which we should report.  The BAM filename is expected to be the
    first token in a whitespace-delimited file."""
    subsetList = dict()
    fp = open(subsetFilename)
    for line in fp:
        itemForSubset = line.rstrip().split()[0]
        subsetList[itemForSubset] = 1
    return(subsetList)


def getFilenameAndMd5sum(metadataDirOfDirs, submission):
    """Given the components of the pathname for a submission, retrieve the
    BAM filename and MD5 checksum from the metadata"""
    analysisPathname = metadataDirOfDirs + "/" + submission + "/analysis.xml"
    analysis = ET.parse(analysisPathname).getroot()
    fileObject = analysis.find("ANALYSIS/DATA_BLOCK/FILES/FILE")
    filename = fileObject.get("filename")
    md5sum = fileObject.get("checksum")
    assert(fileObject.get("checksum_method") == "MD5")
    return((filename, md5sum))

def getRunAccession(metadataDirOfDirs, submission):
    """Given the submission UUID and the metadata directories, retrieve
    the SRA Run accession"""
    runPathname = metadataDirOfDirs + "/" + submission + "/run.xml"
    if not os.path.exists(runPathname):
        return("Missing")
    else:
        runSet = ET.parse(runPathname).getroot()
        firstRunObj = runSet.find("RUN")
        return(firstRunObj.get("accession"))

def getSampleBarcode(metadataDirOfDirs, submission):
    """Given the components of the pathname for a submission, retrieve the
    sample barcode from the analysis file"""
    barcode = "unknown"
    analysisPathname = metadataDirOfDirs + "/" + submission + "/analysis.xml"
    analysis = ET.parse(analysisPathname).getroot()
    targetObj = analysis.find("ANALYSIS/TARGETS/IDENTIFIERS/EXTERNAL_ID")
    if targetObj is not None:
        barcode = targetObj.text
    return(barcode)


if __name__ == '__main__':
    main( )

