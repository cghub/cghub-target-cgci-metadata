#!/usr/bin/perl
#extract *first* library_strategy from SRA xml metadata
use strict;

my $dir = shift;
my $ids = shift;

my @ids;
open(IN,"<$ids");
while(my $line = <IN>)
{
	chomp($line);
	push(@ids,$line);
}
close(IN);

foreach my $srx (@ids)
{
	my @xmls = `ls $dir/$srx*.xml`;
	chomp(@xmls);
	my @lib_strats;
	foreach my $xml (@xmls)
	{
		my $lib_strat = process_xml($xml);
		next if(length($lib_strat) == 0);
		push(@lib_strats,$lib_strat);
	}
	my $size = scalar @lib_strats;
	my $lib_strats = ($size == 0?"":($size > 1?join(":",@lib_strats):$lib_strats[0]));
	print "$srx\t$lib_strats\n";
}

sub process_xml
{
	my $xml = shift;
	my $ls="";
	open(IN,"<$xml");
	while(my $line = <IN>)
	{
		chomp($line);
		if($line =~ /<LIBRARY_STRATEGY>([^<]+)/i)
		{	
			$ls = $1;
			last;
		}
	}
	close(IN);
	return $ls;
}
