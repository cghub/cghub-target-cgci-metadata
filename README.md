This directory is for the metadata curation associated importing
TARGET and CGCI submissions from SRA into CGHub.  This directory contains the
following subdirectories:
    inputs: external inputs to the submissions outside of the metadata itself,
            such as the SRA dbGaP manifest and a file of project accessions.
    scripts: the scripts used in the metadata curation
    metadata: the metadata itself.  Within this directory there is one subdirectory
              per center.  With the exception of BI, whose metadata is processed 
              through a distinct pipeline, it contains the following files and
              subdirectories:
        NCBI_SRA_Metadata_Full_<center>: this ia a per-center metadata dump from
                                         SRA, with the metadata organized by
                                         SRA submissions (which are very different
                                         from CGHub submissions, because they can
                                         be a larger set of unrelated experiments
                                         submitted on the same day.
        perObject: contains the subdirectories experiment, run, and analysis, each
                   of which contain the individual metadata objects of that type.
        repackaged: contains the subdirectory TARGET, plus the subdirectory CGCI
                    if the center also submits for CGCI.  Within each of these 
                    subdirectories, there is a subdirectory with the name of the
                    center (e.g. repackaged/TARGET/BCM).  That directory contains
                    the metadata packaged into individual submissions, where each
                    is in a subdirectory named by a UUID.
        makefile: the file to put it all together

For information on specific scripts, see the scripts subdirectory.
