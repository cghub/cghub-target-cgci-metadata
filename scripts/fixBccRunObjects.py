#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)


 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("perObjectDir", type=str, 
                        help="Directory with each type of metadata object") 
    args = parser.parse_args()

    for runFile in os.listdir(args.perObjectDir + "/run"):
        runPath = args.perObjectDir + "/run/" + runFile
        runTree = readXmlFile(runPath)
        runXml = runTree.getroot()
        experimentRef = runXml.find("EXPERIMENT_REF")
        experimentAccession = experimentRef.get("accession")
        experimentPath = args.perObjectDir + "/experiment/" + experimentAccession + ".xml"
        experiment = readXmlFile(experimentPath).getroot()
        refcenter = experiment.get("center_name")
        experimentRef.set("refcenter", refcenter)
        refname = experiment.get("alias")
        experimentRef.set("refname", refname)
        print "set experiment_ref refname to", refname
        runTree.write(runPath)
        print "wrote to", runPath
        
if __name__ == '__main__':
    main()

    

