#!/usr/bin/perl
use strict;
use warnings;

my $pendingF = shift;
my $errorsF = shift;
my $releasedF = shift;
my $cgciF = shift;

main();

sub main
{
	my $errorsM = load($errorsF);
	my $releasedM = load($releasedF);
	my $cgciM = load($cgciF);
	process_pending($pendingF,$errorsM,$releasedM,$cgciM);
}

sub process_pending
{
	my ($pendingF,$errorsM,$releasedM,$cgciM) = @_;
	open(IN,"<$pendingF");
	print "status\tdetails\tpath\n";
	while(my $line = <IN>)
	{
		chomp($line);
		my @f=split(/\//,$line);
		my $f = pop(@f);
		$f =~ s/\.bam$//;
		if($f=~/^(GS\d+-DNA_\w\w\w)_.+$/)
		{
			$f = $1;
		}
		#following assumes the maps are fully disjoint
		my $type = "not_released";
		$type = "released" if($releasedM->{$f});	
		$type = "CGCI" if($cgciM->{$f});
		$type = $errorsM->{$f} if($errorsM->{$f});
	
		my $details = "";
		if($type =~ /^(.+); (.+)$/)
		{
			$type = $1;	
			$details = $2;
		}

		print "$type\t$details\t$line\n";
	}
	close(IN);
}

sub load
{
	my $file = shift;
	my %map;
	open(IN,"<$file");
	while(my $line = <IN>)
	{
		chomp($line);
		my @f = split(/\t/,$line);
		my $key = shift @f;
		my @k = split(/\//,$key);
		$key = pop(@k);
		$key =~ s/\.bam$//;
		my $val = join("\t",@f);
		if(!$val || $val eq "")
		{
			$val = 1;
		}
		$map{$key}=$val;
	}
	close(IN);
	return \%map;
}
