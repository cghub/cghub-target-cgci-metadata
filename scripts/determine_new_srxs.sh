#!/bin/bash
#get the SRA metadata for any new SRXs (not seen before) and update the library_strategy tracking list for them

SCRIPTS=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SRA_METADATA_DIR=$2

cut -f 11 $1 | egrep -v -e '^-' | egrep -v -e '^Experiment' | sort -u > $SRA_METADATA_DIR/all_target_cgci_srxs
#ls $SRA_METADATA_DIR/SRX*.xml | perl -ne 'chomp; $s=$_; @f=split(/\//,$s); $s=~/(SRX[^\.]+)/; $srx=$1; print "$srx\n";' | sort -u > $SRA_METADATA_DIR/current_target_cgci_srxs
cut -f 1 $SRA_METADATA_DIR/all_target_cgci_srxs.lib_strats | sort -u > $SRA_METADATA_DIR/current_target_cgci_srxs
comm -2 -3 $SRA_METADATA_DIR/all_target_cgci_srxs $SRA_METADATA_DIR/current_target_cgci_srxs > $SRA_METADATA_DIR/needed_target_cgci_srxs
cd $SRA_METADATA_DIR/ ; $SCRIPTS/download_sra_metadata.sh ./needed_target_cgci_srxs
$SCRIPTS/get_library_strategy_from_sra_xmls.pl $SRA_METADATA_DIR needed_target_cgci_srxs >> $SRA_METADATA_DIR/all_target_cgci_srxs.lib_strats
$SCRIPTS/get_barcode_study_from_sra_xmls.pl $SRA_METADATA_DIR needed_target_cgci_srxs >> $SRA_METADATA_DIR/all_target_cgci_srxs.barcodes_studies_tss_codes
sort -u $SRA_METADATA_DIR/all_target_cgci_srxs.lib_strats | egrep -v -e '	$'  | egrep -e '	' > $SRA_METADATA_DIR/all_target_cgci_srxs.lib_strats.sorted
sort -u $SRA_METADATA_DIR/all_target_cgci_srxs.barcodes_studies_tss_codes | egrep -v -e '	$'  | egrep -e '	' > $SRA_METADATA_DIR/all_target_cgci_srxs.barcodes_studies_tss_codes.sorted
mv $SRA_METADATA_DIR/all_target_cgci_srxs.lib_strats.sorted $SRA_METADATA_DIR/all_target_cgci_srxs.lib_strats
mv $SRA_METADATA_DIR/all_target_cgci_srxs.barcodes_studies_tss_codes.sorted $SRA_METADATA_DIR/all_target_cgci_srxs.barcodes_studies_tss_codes
