#!/usr/bin/env python

import argparse, re, os, sys, csv
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    from xml.etree import ElementTree

def cmdParse():
    "parse command line and return arguments object"
    parser = argparse.ArgumentParser(description="""Take an file of SRA XML format metadata for TARGET and generate a TSV file with a subset of information need by analysis programs.""")
    parser.add_argument("sraXml", type=str,
                        help="""file containing SRA XML for all of TARGET.  The easiest way to obtain this is with the cgquery command: cgquery -Aa -o target.sra.xml 'study=(phs0004* OR phs000218)'""")
    parser.add_argument("cghubStatusTsv", type=str,
                        help="""status information is written to this file.""")
    return parser.parse_args()

statusTsvHeaders = ("analysis_id", "study", "center", "state", "sample_barcode", "expr_accessions", "run_accessions", "files_info")

def emptyIfNone(val):
    if val == None:
        return ""
    else:
        return val

def writeRow(fh, row):
    fh.write('\t'.join(emptyIfNone(col) for col in row)+'\n')

def etGet(node, elementSpec):
    "find an element in under an ElementTree node, generating an error if not found"
    el = node.find(elementSpec)
    if el == None:
        raise Exception("can't find XML element: " + elementSpec)
    return el

def etGets(node, elementSpec):
    "find element(s) in under an ElementTree node, generating an error if not found"
    els = node.findall(elementSpec)
    if els == None or len(els) == 0:
        raise Exception("can't find any XML elements: " + elementSpec)
    return els

def etGetTextOrError(node, elementSpec):
    "get the text of element under an ElementTree node, an error string if not found"
    text = etGet(node, elementSpec).text
    if text == None:
        raise Exception("no text for XML element: " + elementSpec)
    return text

def etGetTextOrEmpty(node, elementSpec, multiples=False):
    "get the text of element under an ElementTree node, an empty string if not found"
    if multiples:
       els = etGets(node, elementSpec)
       texts = set()
       [texts.add(el.text) for el in els]
       return texts
    text = etGet(node, elementSpec).text
    if text == None:
        return ""
    return text

def etFindTextOrEmpty(node, elementSpec):
    "get the text of element under an optional ElementTree node, an empty string if not found"
    element = node.find(elementSpec)
    if (element != None) and (element.text != None):
        return element.text
    else:
        return ""

def getAccessions(resultTree, elementPattern):
    """get either run or experiment accessions from all matching elements,
    sorted list is returned"""
    accs = []
    for elem in resultTree.findall(elementPattern):
        acc = elem.attrib.get("accession")
        if (acc != None) and (acc not in accs):
            accs.append(acc)
    return sorted(accs)

#get all files' info tuples (filename,filesize,checksum) and flatten into delimited string
def find_file_info(resultTree):
    files_ = etGets(resultTree, "./files/file")
    files = {}
    for file in files_:
        files[file.find("filename").text]=[file.find("filesize").text,file.find("checksum").text]
    filenames = ",".join(map(lambda x: "%s:%s:%s" % (x,files[x][0],files[x][1]),[x for x in files.keys() if ".bai" not in x]))
    return filenames

def processResultSra(resultTree, statusTsvFh):
    analysisId = etGetTextOrEmpty(resultTree, "./analysis_id")
    study = etGetTextOrError(resultTree, "./study")
    center = etGetTextOrEmpty(resultTree, "./center_name")
    state = etGetTextOrEmpty(resultTree, "./state")
    sampleBarcode = etGetTextOrEmpty(resultTree, "./legacy_sample_id")
    runAccessions = getAccessions(resultTree, ".//RUN")
    exprAccessions = getAccessions(resultTree, ".//EXPERIMENT")
    files = find_file_info(resultTree)
    writeRow(statusTsvFh, [analysisId, study, center, state, sampleBarcode, 
                           ",".join(exprAccessions), ",".join(runAccessions), files])

def processAllSra(allSraTree, statusTsvFh):
    writeRow(statusTsvFh, statusTsvHeaders)
    for resultTree in allSraTree.findall("./Result"):
        processResultSra(resultTree, statusTsvFh)

args = cmdParse()
allSraTree = ElementTree.parse(args.sraXml)
with open(args.cghubStatusTsv, "w") as statusTsvFh:
    processAllSra(allSraTree, statusTsvFh)
