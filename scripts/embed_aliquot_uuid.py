#!/usr/bin/env python

import argparse
from functools import partial
import logging
import tempfile
import textwrap
import MySQLdb
import MySQLdb.cursors
from lxml import etree
import os
import uuid
import sys

log = logging.getLogger( )
handler = logging.StreamHandler( sys.stderr )
handler.setFormatter( logging.Formatter( '%(levelname)s: %(message)s' ) )
log.addHandler( handler )
log.setLevel( logging.INFO )


def parse_args( args ):
    parser = argparse.ArgumentParser( )
    parser.add_argument( 'dir',
                         help='Path to input directory.' )
    parser.add_argument( '--db', '-D', metavar='DATABASE',
                         default='cghub_dev',
                         help="Name of the metadata database that contains the mapping from "
                              "barcode to aliquot UUID." )
    parser.add_argument( '--db-host', '-H', metavar='HOST',
                         default='app01',
                         help="Host name of the metadata database." )
    parser.add_argument( '--db-user', '-U', metavar='USER',
                         default='readonly',
                         help="User to connect to the database as." )
    parser.add_argument( '--dry-run', action='store_true', default=False,
                         help="Don't write the modified XML back to the file." )
    parser.add_argument( '--dont-embed', action='store_true', default=False,
                         help="Don't make any semantic changes, just parse the XML and write it "
                              "out again, limiting the changes to reencoding the file in UTF-8, "
                              "quoting unsafe characters, normalizing whitespace and forcing "
                              "double quotes for attribute delimiters." )
    parser.add_argument( '--force-mapping', action='store_true', default=False,
                         help="If the input file already contains an aliquot UUID but that UUID "
                              "is inconsistent with the mapping, replace the existing UUID in the "
                              "input file with the one from the mapping. The default is to not "
                              "replace an existing aliquot UUIDs but this will result in a submission "
                              "that will fail during the validating_sample state. This will "
                              "affect the subset of BI submissions not submitted in May 2013." )
    options = parser.parse_args( args )
    return options


def main( ):
    options = parse_args( sys.argv[ 1: ] )
    main = Main( options )
    main.load_mapping( )
    main.walk_tree( )


class Main( object ):
    """
    The main workhorse
    """

    def __init__( self, options ):
        super( Main, self ).__init__( )
        self.options = options

    aliquot_id_by_barcode = { }
    barcode_by_aliquot_id = { }

    def add_mapping( self, aliquot_id, barcode ):
        aliquot_id = uuid.UUID( aliquot_id )
        self.aliquot_id_by_barcode[ barcode ] = aliquot_id
        self.barcode_by_aliquot_id[ aliquot_id ] = barcode

    def load_mapping( self ):
        connection = MySQLdb.connect( host=self.options.db_host,
                                      user=self.options.db_user,
                                      db=self.options.db,
                                      cursorclass=MySQLdb.cursors.SSCursor )
        try:
            cursor = connection.cursor( )
            try:
                query = textwrap.dedent( """
                    # Get the most recent barcode per aliquot_id:
                    SELECT b1.aliquot_id, b1.barcode
                    FROM dcc_target_barcodes b1
                    LEFT JOIN dcc_target_barcodes b2 ON b1.aliquot_id = b2.aliquot_id AND b1.release < b2.release
                    WHERE b2.aliquot_id IS NULL""" )
                cursor.execute( query )
                for row in cursor:
                    aliquot_id, barcode = row
                    self.add_mapping( aliquot_id, barcode )
            finally:
                cursor.close( )
        finally:
            connection.close( )

    def walk_tree( self ):
        for dir_path, dir_names, file_names in os.walk( self.options.dir ):
            dir_name = os.path.basename( dir_path )
            if dir_name.endswith( '.error' ):
                dir_name = dir_name[ :-6 ]
            analysis_id = parse_uuid( dir_name )
            if analysis_id:
                Submission( self, dir_path, file_names ).process( )


class Submission( object ):
    parser = etree.XMLParser( remove_blank_text=True )

    def __init__( self, parent, dir_path, file_names ):
        self.parent = parent
        self.dir_path = dir_path
        self.file_names = file_names
        self.barcode_patch = None
        super( Submission, self ).__init__( )

    def process( self ):
        for file_name, transformation in [
            ( 'analysis.xml', self.embed_aliquot_id ), # must be first, to populate barcode_patch
            ( 'experiment.xml', partial( self.patch_barcode, 'EXPERIMENT' )  ),
            ( 'run.xml', partial( self.patch_barcode, 'RUN' )  ), 
            ( 'manifest.xml', partial( self.patch_barcode, 'manifest',
                                       warnIfMissing=False )  ) ]:
            self.process_file( file_name, transformation )

    def process_file( self, file_name, transformation, warnIfMissing=True ):
        if file_name in self.file_names:
            try:
                path = os.path.join( self.dir_path, file_name )
                with open( path ) as input_file:
                    xml = etree.parse( input_file, self.parser )
                    if not self.parent.options.dont_embed:
                        transformation( xml )
                if not self.parent.options.dry_run:
                    with tempfile.NamedTemporaryFile( prefix=file_name + '.',
                                                      dir=self.dir_path,
                                                      delete=False ) as temp_file:
                        xml.write( temp_file,
                                   pretty_print=True,
                                   encoding='UTF-8',
                                   xml_declaration=True )
                    os.rename( temp_file.name, path )
            except self.EmbedError as e:
                log.warn( e.message )
        else:
            if not warnIfMissing:
                log.warn( '%s missing from submission' % file_name )

    study_xpath = etree.XPath( '/ANALYSIS_SET/ANALYSIS/STUDY_REF/@refname' )
    target_xpath = etree.XPath( '/ANALYSIS_SET/ANALYSIS/TARGETS/TARGET[@sra_object_type="SAMPLE"]' )
    external_id_xpath = etree.XPath( './EXTERNAL_ID[@label="NCI_TARGET"]' )

    pre2013_barcode_renamings = {
        'TARGET-20-PAEAKL-01-01D': ( 'TARGET-20-PAEAKL-09A-01D', '20120705' ),
        'TARGET-20-PAEAKL-10-01D': ( 'TARGET-20-PAEAKL-10A-01D', '20120705' ),
        'TARGET-20-PAKIYW-01-01D': ( 'TARGET-20-PAKIYW-09A-01D', '20120705' ),
        'TARGET-20-PAKIYW-14-01D': ( 'TARGET-20-PAKIYW-14A-01D', '20120705' ),
        'TARGET-20-PAKRZG-01-01D': ( 'TARGET-20-PAKRZG-09A-01D', '20120705' ),
        'TARGET-20-PAKRZG-14-01D': ( 'TARGET-20-PAKRZG-14A-01D', '20120705' ),
        'TARGET-20-PANCSC-01-01D': ( 'TARGET-20-PANCSC-09A-01D', '20120705' ),
        'TARGET-20-PANCSC-14-01D': ( 'TARGET-20-PANCSC-14A-01D', '20120705' ),
        'TARGET-20-PANUTB-01-01D': ( 'TARGET-20-PANUTB-09A-01D', '20120705' ),
        'TARGET-20-PANUTB-14-01D': ( 'TARGET-20-PANUTB-14A-01D', '20120705' ),
        'TARGET-20-PARHVK-01-01D': ( 'TARGET-20-PARHVK-09A-01D', '20120705' ),
        'TARGET-20-PARHVK-14-01D': ( 'TARGET-20-PARHVK-14A-01D', '20120705' ),
        'TARGET-20-PARUTH-01-01D': ( 'TARGET-20-PARUTH-09A-01D', '20120705' ),
        'TARGET-20-PARUTH-14-01D': ( 'TARGET-20-PARUTH-14A-01D', '20120705' ),
        'TARGET-20-PARZUU-01-01D': ( 'TARGET-20-PARZUU-09A-01D', '20120705' ),
        'TARGET-20-PARZUU-14-01D': ( 'TARGET-20-PARZUU-14A-01D', '20120705' ),
        'TARGET-20-PASBPK-01-01D': ( 'TARGET-20-PASBPK-09A-01D', '20120705' ),
        'TARGET-20-PASBPK-14-01D': ( 'TARGET-20-PASBPK-14A-01D', '20120705' ),
        'TARGET-20-PASIBG-01-01D': ( 'TARGET-20-PASIBG-09A-01D', '20120705' ),
        'TARGET-20-PASIBG-14-01D': ( 'TARGET-20-PASIBG-14A-01D', '20120705' ),
        'TARGET-20-PAEEYP-03A-01D': ( 'TARGET-20-PAEEYP-09A-01D', '20120705' ),
        'TARGET-20-PAERAH-03A-01D': ( 'TARGET-20-PAERAH-09A-01D', '20120705' ),
        'TARGET-20-PAKKBK-03A-01D': ( 'TARGET-20-PAKKBK-09A-01D', '20120705' ),
        'TARGET-20-PAMYAS-03A-01D': ( 'TARGET-20-PAMYAS-09A-01D', '20120705' ),
        'TARGET-20-PANGTF-03A-01D': ( 'TARGET-20-PANGTF-09A-01D', '20120705' ),
        'TARGET-20-PANLIZ-03A-01D': ( 'TARGET-20-PANLIZ-09A-01D', '20120705' ),
        'TARGET-20-PANSBH-03A-01D': ( 'TARGET-20-PANSBH-09A-01D', '20120705' ),
        'TARGET-20-PANTWV-03A-01D': ( 'TARGET-20-PANTWV-09A-01D', '20120705' ),
        'TARGET-20-PATELT-03A-01D': ( 'TARGET-20-PATELT-09A-01D', '20120705' ),
        'TARGET-20-PANVGP-03A-01D': ( 'TARGET-20-PANVGP-09A-01D', '20120705' ),
        'TARGET-20-PARIEG-03A-01D': ( 'TARGET-20-PARIEG-09A-01D', '20120705' ),
        'TARGET-20-PARUCB-03A-01D': ( 'TARGET-20-PARUCB-09A-01D', '20120705' ),
        'TARGET-20-PARUNX-03A-01D': ( 'TARGET-20-PARUNX-09A-01D', '20120705' ),
        'TARGET-20-PARWAS-03A-01D': ( 'TARGET-20-PARWAS-09A-01D', '20120705' ),
        'TARGET-20-PARWXU-03A-01D': ( 'TARGET-20-PARWXU-09A-01D', '20120705' ),
        'TARGET-20-PASFEW-03A-01D': ( 'TARGET-20-PASFEW-09A-01D', '20120705' ),
        'TARGET-20-PANLRE-03A-01D': ( 'TARGET-20-PANLRE-09A-01D', '20120705' ),
        'TARGET-20-PASRTP-03A-01D': ( 'TARGET-20-PASRTP-09A-01D', '20120705' ),
        'TARGET-20-PATABB-03A-01D': ( 'TARGET-20-PATABB-09A-01D', '20120705' ),
        'TARGET-30-PAPSKM-01A-01D': ( 'TARGET-30-PAPSKM-01A-01W', '20121116' ),
        'TARGET-30-PARIRD-01A-01D': ( 'TARGET-30-PARIRD-01A-01W', '20121116' ),
        'TARGET-30-PAPSKM-10A-01D': ( 'TARGET-30-PAPSKM-10A-01W', '20121116' ),
        'TARGET-30-PARGUX-01A-01D': ( 'TARGET-30-PARGUX-01A-01W', '20121116' ),
        'TARGET-30-PARIRD-10A-01D': ( 'TARGET-30-PARIRD-10A-01W', '20121116' ),
        'TARGET-30-PAPTMM-10A-01D': ( 'TARGET-30-PAPTMM-10A-01W', '20121116' ),
        'TARGET-30-PAPTLD-10A-01D': ( 'TARGET-30-PAPTLD-10A-01W', '20121116' ),
        'TARGET-30-PASAZJ-10A-01D': ( 'TARGET-30-PASAZJ-10A-01W', '20121116' ),
        'TARGET-30-PARGUX-10A-01D': ( 'TARGET-30-PARGUX-10A-01W', '20121116' ),
        'TARGET-30-PAPTMM-01A-01D': ( 'TARGET-30-PAPTMM-01A-01W', '20121116' ),
        'TARGET-30-PAREGK-01A-01D': ( 'TARGET-30-PAREGK-01A-01W', '20121116' ),
        'TARGET-30-PARDUJ-10A-01D': ( 'TARGET-30-PARDUJ-10A-01W', '20121116' ),
        'TARGET-30-PAPTAN-10A-01D': ( 'TARGET-30-PAPTAN-14A-01D', '20121116' ),
        'TARGET-30-PAREGK-10A-01D': ( 'TARGET-30-PAREGK-10A-01W', '20121116' ),
        'TARGET-30-PARDUJ-01A-01D': ( 'TARGET-30-PARDUJ-01A-01W', '20121116' ),
        'TARGET-30-PASAZJ-01A-01D': ( 'TARGET-30-PASAZJ-01A-01W', '20121116' ),
        'TARGET-30-PAPTLD-01A-01D': ( 'TARGET-30-PAPTLD-01A-01W', '20121116' ),
    }

    def embed_aliquot_id( self, analysis_xml ):
        """

        Happy path:

        >>> import lxml.usedoctest
        >>> m = Main( options=parse_args( [ '' ] ) )
        >>> m.add_mapping( '17c29fa8-e2a8-4c21-bcc3-5fffa1049968', 'TARGET-30-PATILE-01A-01D' )
        >>> s = Submission( m, None, None )
        >>> xml = etree.XML( '''
        ...	<ANALYSIS_SET>
        ...     <ANALYSIS>
        ...         <STUDY_REF refname="phs000467"/>
        ...         <TARGETS>
        ...             <TARGET sra_object_type="SAMPLE" refname="TARGET-30-PATILE-01A-01D" />
        ...         </TARGETS>
        ...         </ANALYSIS>
        ... </ANALYSIS_SET>''' )
        >>> s.embed_aliquot_id( xml )
        >>> print etree.tostring(xml)
        <ANALYSIS_SET>
            <ANALYSIS>
                <STUDY_REF refname="phs000467"/>
                <TARGETS>
                    <TARGET sra_object_type="SAMPLE" refname="17c29fa8-e2a8-4c21-bcc3-5fffa1049968" />
                    <IDENTIFIERS>
                        <EXTERNAL_ID namespace="phs000467" label="NCI_TARGET">TARGET-30-PATILE-01A-01D</EXTERNAL_ID>
                    </IDENTIFIERS>
                </TARGETS>
            </ANALYSIS>
        </ANALYSIS_SET>

        Idempotence:

        >>> xml = etree.XML( '''
        ...	<ANALYSIS_SET>
        ...     <ANALYSIS>
        ...         <STUDY_REF refname="phs000467"/>
        ...         <TARGETS>
        ...             <TARGET sra_object_type="SAMPLE" refname="17c29fa8-e2a8-4c21-bcc3-5fffa1049968" />
        ...             <IDENTIFIERS>
        ...                 <EXTERNAL_ID namespace="phs000467" label="NCI_TARGET">TARGET-30-PATILE-01A-01D</EXTERNAL_ID>
        ...                 <UUID>17c29fa8-e2a8-4c21-bcc3-5fffa1049968</UUID>
        ...             </IDENTIFIERS>
        ...         </TARGETS>
        ...         </ANALYSIS>
        ... </ANALYSIS_SET>''' )
        >>> s.embed_aliquot_id( xml )
        >>> print etree.tostring(xml)
        <ANALYSIS_SET>
            <ANALYSIS>
                <STUDY_REF refname="phs000467"/>
                <TARGETS>
                    <TARGET sra_object_type="SAMPLE" refname="17c29fa8-e2a8-4c21-bcc3-5fffa1049968" />
                    <IDENTIFIERS>
                        <EXTERNAL_ID namespace="phs000467" label="NCI_TARGET">TARGET-30-PATILE-01A-01D</EXTERNAL_ID>
                        <UUID>17c29fa8-e2a8-4c21-bcc3-5fffa1049968</UUID>
                    </IDENTIFIERS>
                </TARGETS>
            </ANALYSIS>
        </ANALYSIS_SET>

        Override UUID from mapping:

        >>> m.options.force_mapping = True
        >>> xml = etree.XML( '''
        ...	<ANALYSIS_SET>
        ...     <ANALYSIS>
        ...         <STUDY_REF refname="phs000467"/>
        ...         <TARGETS>
        ...             <TARGET sra_object_type="SAMPLE" refname="164d9227-7fe2-5443-bbdc-8d6397a1a080" />
        ...             <IDENTIFIERS>
        ...                 <EXTERNAL_ID namespace="phs000467" label="NCI_TARGET">TARGET-30-PATILE-01A-01D</EXTERNAL_ID>
        ...                 <UUID>164d9227-7fe2-5443-bbdc-8d6397a1a080</UUID>
        ...             </IDENTIFIERS>
        ...         </TARGETS>
        ...         </ANALYSIS>
        ... </ANALYSIS_SET>''' )
        >>> s.embed_aliquot_id( xml )
        >>> print etree.tostring(xml)
        <ANALYSIS_SET>
            <ANALYSIS>
                <STUDY_REF refname="phs000467"/>
                <TARGETS>
                    <TARGET sra_object_type="SAMPLE" refname="17c29fa8-e2a8-4c21-bcc3-5fffa1049968" />
                    <IDENTIFIERS>
                        <EXTERNAL_ID namespace="phs000467" label="NCI_TARGET">TARGET-30-PATILE-01A-01D</EXTERNAL_ID>
                        <EXTERNAL_ID label="Legacy aliquot UUID" namespace="http://www.broadinstitute.org/">164d9227-7fe2-5443-bbdc-8d6397a1a080</EXTERNAL_ID>
                        <UUID>17c29fa8-e2a8-4c21-bcc3-5fffa1049968</UUID>
                    </IDENTIFIERS>
                </TARGETS>
            </ANALYSIS>
        </ANALYSIS_SET>
        """
        rel_path = self.rel_path( analysis_xml )

        # Get handle on relevant elements
        study = get_one( self.study_xpath( analysis_xml ) )
        target = get_one( self.target_xpath( analysis_xml ) )
        targets = target.getparent( )

        # Extract TARGET@refname and see if it is a UUID
        refname = target.get( 'refname' )
        if not refname:
            log.warn(
                "TARGET@refname attribute is missing or empty in '%s'." % rel_path )
        else:
            aliquot_id = parse_uuid( refname )
            if aliquot_id is None:
                # Not a UUID, so it must be a barcode.
                barcode = self.read_barcode( analysis_xml, target )
                # Look up the UUID and set TARGET@refname to it
                aliquot_id = self.lookup_aliquot_id( barcode, rel_path )
                target.set( 'refname', str( aliquot_id ) )
            else:
                # Make a note that this was a pre-existing UUID
                barcode = None

            # Get or create TARGETS/IDENTIFIERS
            identifiers = get_zero_or_one( targets.findall( 'IDENTIFIERS' ) )
            if identifiers is None:
                identifiers = etree.SubElement( targets, 'IDENTIFIERS' )

            # Get or create the IDENTIFIER element that contains the barcode
            external_id = get_zero_or_one( self.external_id_xpath( identifiers ) )
            if external_id is None:
                external_id = etree.SubElement( identifiers, 'EXTERNAL_ID',
                                                label="NCI_TARGET",
                                                namespace=study )

            # Ensure that namespace matches study
            namespace = external_id.get( 'namespace' )
            if namespace != study:
                raise RuntimeError( 'Namespace should be {0} not, {1}'.format( study, namespace ) )

            # Is barcode already present?
            if external_id.text:
                # Was the aliquot UUID already there?
                if barcode is None:
                    # Check whether its is consistent with mapping
                    barcode = external_id.text
                    mapped_aliquot_id = self.lookup_aliquot_id( barcode, rel_path )
                    if mapped_aliquot_id != aliquot_id:
                        # If not, either enforce or just warn about it
                        if self.parent.options.force_mapping:
                            log.info( 'Replacing embedded aliquot ID {0} with {1} in {2}.'.format(
                                    aliquot_id, mapped_aliquot_id, rel_path ) )
                            old_aliquot_id = etree.SubElement( identifiers, 'EXTERNAL_ID',
                                                               label='Legacy aliquot UUID',
                                                               namespace='http://www.broadinstitute.org/' )
                            old_aliquot_id.text = str( aliquot_id )
                            aliquot_id = mapped_aliquot_id
                            target.set( 'refname', str( aliquot_id ) )
                        else:
                            log.warn( 'Embedded aliquot ID {0} should be {1} in {2}.'.format(
                            aliquot_id, mapped_aliquot_id, rel_path ) )
                elif external_id.text != barcode:
                    raise RuntimeError( )
            else:
                # Barcode not present, just set it
                external_id.text = self.lookup_barcode( aliquot_id, rel_path )

            # Enforce that IDENTIFIERS/UUID, if present is consistent with TARGET@refname
            _uuid = identifiers.find( 'UUID' )
            if _uuid is not None:
                _uuid.text = str( aliquot_id )

            identifiers_order = [ 'PRIMARY_ID', 'SECONDARY_ID', 'EXTERNAL_ID', 'SUBMITTER_ID', 'UUID' ]
            identifiers[ : ] = sorted( identifiers, key=lambda x: identifiers_order.index( x.tag ) )

    def read_barcode( self, analysis_xml, target ):
        """
        This method gets the barcode from the refname attribute of the given target element and
        checks if it is one of those barcodes that were changed in 2012. If so it patches all
        known occurrences of that barcode in the input XML to the new barcode.

        :param analysis_xml: the input XML
        :param target: the ANALYSIS/TARGETS/TARGET element
        """
        barcode = target.get( 'refname' )
        # If it one of those  ...
        if barcode in self.pre2013_barcode_renamings:
            # ... look up the replacement barcode and patch every known occurrence. This code is
            # very ad-hoc but we have a generic test that tells us if we we missed anything.
            pre2013_barcode = barcode
            barcode = self.pre2013_barcode_renamings[ pre2013_barcode ][ 0 ]
            log.info( 'Replacing pre-2013 barcode {0} with {1} in {2}'.format(
                pre2013_barcode, barcode, self.rel_path( analysis_xml ) ) )
            target.set( 'refname', barcode )
            self.barcode_patch = ( pre2013_barcode, barcode )
            self.patch_barcode( 'ANALYSIS', analysis_xml )
        return barcode

    def rel_path( self, xml ):
        try:
            xml = xml.getroottree()
        except AttributeError:
            pass
        return os.path.relpath( xml.docinfo.URL, self.parent.options.dir )

    def patch_barcode( self, element_name, xml, warnIfMissing=True ):
        if self.barcode_patch:
            for element in xml.findall( element_name ):
                self._patch_barcode( element )
            self._verify_patched_barcode( xml )

    def _patch_barcode( self, element ):
        old_barcode, new_barcode = self.barcode_patch
        alias = element.get( 'alias' )
        alias = alias.replace( old_barcode, new_barcode )
        element.set( 'alias', alias )
        for title in element.findall( 'TITLE' ):
            title.text = title.text.replace( old_barcode, new_barcode )
        for submitter_id in element.findall( 'IDENTIFIERS/SUBMITTER_ID' ):
            submitter_id.text = submitter_id.text.replace( old_barcode, new_barcode )
        for description in element.findall( 'DESCRIPTION' ):
            if description.text is not None:
                description.text = description.text.replace( old_barcode, new_barcode )
        for sequence in element.findall( 'ANALYSIS_TYPE/REFERENCE_ALIGNMENT/SEQ_LABELS/SEQUENCE' ):
            if sequence.get("data_block_name") is not None:
                sequence.set("data_block_name", sequence.get("data_block_name").replace( old_barcode, new_barcode))
        for file_ in element.findall( 'DATA_BLOCK/FILES/FILE' ):
            old_filename = file_.get( 'filename' )
            if old_barcode in old_filename:
                new_filename = old_filename.replace( old_barcode, new_barcode )
                log.warn( 'Changing filename {0} to {1} in {2}'.format(
                    old_filename, new_filename, self.rel_path( element ) ) )
                file_.set( 'filename', new_filename )
        for sample_descriptor in element.findall( 'DESIGN/SAMPLE_DESCRIPTOR' ):
            if sample_descriptor.get("refname") is not None:
                sample_descriptor.set("refname", sample_descriptor.get("refname").replace( old_barcode, new_barcode ))
                # double check that we didn't miss any occurrences of the pre-2013 barcode

    def _verify_patched_barcode( self, xml ):
        old_barcode, _ = self.barcode_patch
        for match in xml.xpath( '//text()|//@*' ):
            if old_barcode in match:
                parent = match.getparent( )
                path = parent.getroottree( ).getpath( parent )
                raise RuntimeError( "Didn't catch occurrence of pre-2013 barcode {0} in "
                                    "{1} of {2}".format( old_barcode, path, self.rel_path( xml ) ) )

    class EmbedError( Exception ):
        pass

    def lookup_aliquot_id( self, barcode, rel_path ):
        try:
            return self.parent.aliquot_id_by_barcode[ barcode ]
        except KeyError:
            raise self.EmbedError( 'Unknown barcode {0} in {1}'.format( barcode, rel_path ) )

    def lookup_barcode( self, aliquot_id, rel_path ):
        try:
            return self.parent.barcode_by_aliquot_id[ aliquot_id ]
        except KeyError:
            raise self.EmbedError( 'Unknown aliquot_id {0} in {1}'.format( aliquot_id, rel_path ) )


def parse_uuid( uuid_str ):
    try:
        return uuid.UUID( uuid_str )
    except ValueError:
        return None


def get_first( iterable ):
    """
    >>> get_first([]) is None
    True
    >>> get_first([1])
    1
    >>> get_first([1,2])
    1
    """
    return next( iter( iterable ), None )


def get_zero_or_one( iterable ):
    """
    >>> get_zero_or_one([]) is None
    True
    >>> get_zero_or_one([1])
    1
    >>> get_zero_or_one([1,2])
    Traceback (most recent call last):
    ....
    ValueError: Argument has more than a single element
    """
    iterator = iter( iterable )
    result = next( iterator, None )
    try:
        next( iterator )
    except StopIteration:
        return result
    else:
        raise ValueError( 'Argument has more than a single element' )


def get_one( iterable ):
    """
    >>> get_one([])
    Traceback (most recent call last):
    ....
    ValueError: Argument has no elements
    >>> get_one([1])
    1
    >>> get_one([1,2])
    Traceback (most recent call last):
    ....
    ValueError: Argument has more than a single element
    """
    iterator = iter( iterable )
    try:
        result = next( iterator )
    except StopIteration:
        raise ValueError( 'Argument has no elements' )
    try:
        next( iterator )
    except StopIteration:
        return result
    else:
        raise ValueError( 'Argument has more than a single element' )


if __name__ == '__main__':
    main( )
