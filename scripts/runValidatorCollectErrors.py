#!/usr/bin/env python

import argparse
from collections import OrderedDict
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET


class validationError(object):

    def __init__(self, errorXml):
        self.errmsg = errorXml.text
        self.detail = dict()
        for key in errorXml.keys():
            self.detail[key] = errorXml.get(key)

    def printDetail(self, filepath, filename):
        msg = "%s\t%s\t%s" % (filepath, filename, self.errmsg)
        msg = msg + "\t"
        if self.detail.has_key("xpath"):
            msg = msg + self.detail["xpath"]
        msg = msg + "\t"
        if self.detail.has_key("expected"):
            msg = msg + self.detail["expected"]
        msg = msg + "\t"
        if self.detail.has_key("received"):
            msg = msg + self.detail["received"]
        msg = msg + "\t"
        if self.detail.has_key("line"):
            msg = msg + self.detail["line"]
        print msg


class validationPerFile(object):
    
    def __init__(self, filetype, status="OK"):
        self.filetype = filetype
        self.status = status
        self.errors = list()

    def parseErrors(self, validationMessages):
        for mySection in validationMessages.iter(self.filetype):
            myStatus = mySection.find("status")
            self.status = myStatus.text
            for thisError in mySection.iter("error"):
                self.errors.append(validationError(thisError))

    def printDetail(self, filepath, debug):
        if debug:
            print filepath, self.status, len(self.errors)
        for myError in self.errors:
            myError.printDetail(filepath, self.filetype)



class validationStatus(object):

    def __init__(self, filepath, validationOutputLines, debug=False):
        self.filepath = filepath.split("/")[-1]
        if len(validationOutputLines) == 0:
            self.status = "Failed"
            self.analysis = validationPerFile("analysis", self.status)
            self.run = validationPerFile("run", self.status)
            self.experiment = validationPerFile("experiment", self.status)
            self.analysisRunXref = validationPerFile("analysis_run", self.status)
            self.runExperimentXref = validationPerFile("run_experiment", self.status)
        elif re.match("^OK", validationOutputLines[0]):
            self.status = "OK"
            self.analysis = validationPerFile("analysis", self.status)
            self.run = validationPerFile("run", self.status)
            self.experiment = validationPerFile("experiment", self.status)
            self.analysisRunXref = validationPerFile("analysis_run", self.status)
            self.runExperimentXref = validationPerFile("run_experiment", self.status)
        else:
            self.status = "Failed"
            self.analysis = validationPerFile("analysis", self.status)
            self.run = validationPerFile("run", self.status)
            self.experiment = validationPerFile("experiment", self.status)
            self.analysisRunXref = validationPerFile("analysis_run", self.status)
            self.runExperimentXref = validationPerFile("run_experiment", self.status)
            failureMessages = ET.fromstringlist(validationOutputLines)
            self.analysis.parseErrors(failureMessages) 
            self.run.parseErrors(failureMessages)
            self.experiment.parseErrors(failureMessages)
            self.analysisRunXref.parseErrors(failureMessages)
            self.runExperimentXref.parseErrors(failureMessages)
        if debug:
            print self.filepath, "Status:", self.status
            
    def printSummary(self):
        print "%s\t%s\t%s\t%s\t%s\t%s\t%s" % (self.filepath, self.status, 
                                              self.analysis.status, self.run.status, 
                                              self.experiment.status, 
                                              self.analysisRunXref.status,
                                              self.runExperimentXref.status)

    def printDetail(self, debug=False):
        self.analysis.printDetail(self.filepath, debug)
        self.run.printDetail(self.filepath, debug)
        self.experiment.printDetail(self.filepath, debug)
        self.analysisRunXref.printDetail(self.filepath, debug)
        self.runExperimentXref.printDetail(self.filepath, debug)
        



def runValidator(submissionDir, debug):
    """Run the validator on this submission and return the output"""
    cmd = ("curl -k -v -F analysisxml=@%s/analysis.xml"
           + " -F runxml=@%s/run.xml"
           + " -F experimentxml=@%s/experiment.xml"
           + " https://stage.cghub.ucsc.edu/cghub/metadata/analysis/validate") \
          % (submissionDir, submissionDir, submissionDir)
    if debug:
        print "cgsubmit cmd:", cmd
    proc = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, 
                            stdout=subprocess.PIPE)
    outputLines = list()
    for line in proc.stdout:
        outputLines.append(line)
    status = validationStatus(submissionDir, outputLines, debug)
    return(status)


def printHeader(full):
    if full:
        print "Submission\tStatus\tFile\tError\tXPath\tExpected\tReceived\tLine"
    else:
        print "Submission\tStatus\tAnalysis\tRun\tExperiment\tAnalysisRunXref\tRunExperimentXref"


def reportErrors(results, full=False, debug=False):
    if full:
        results.printDetail(debug)
    else:
        results.printSummary()
                                

def runValidatorReportErrors(submissionDir, full=False, debug=False):
    """Validate a given submission and report any metadata errors"""
    submissionName = submissionDir.split("/")[-1]
    results = runValidator(submissionDir, debug)
    reportErrors(results, full, debug)
    

parser = argparse.ArgumentParser()
parser.add_argument('metadataDirOfDirs', type=str,
                    help="Directory of metadata directories")
parser.add_argument('-d', '--debug', type=str, default=False,
                    help="Display debugging info?")
parser.add_argument('-f', '--full', type=str, default=False,
                    help="Display full output?")
args = parser.parse_args()

printHeader(args.full)
for submissionDir in os.listdir(args.metadataDirOfDirs):
    runValidatorReportErrors(args.metadataDirOfDirs + "/" + submissionDir,
                             args.full, args.debug)
