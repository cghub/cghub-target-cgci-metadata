#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import warnings
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)

def getRunAlias(analysis):
    for runReference in analysis.iter("RUN"):
        runAlias = runReference.get("refname")
        return(runAlias)
                                 

 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("submissionsDir", type=str, 
                        help="""Directory which contains one subdirectory for each submission.  Each subdirectory should contain one analysis.xml, one run.xml and one experiment.xml""")
    args = parser.parse_args()

    for thisSubmission in os.listdir(args.submissionsDir):
        print "working on", thisSubmission
        analysisPath = args.submissionsDir + "/" + thisSubmission + "/analysis.xml"
        runPath = args.submissionsDir + "/" + thisSubmission + "/run.xml"
        if os.path.exists(analysisPath) and os.path.exists(runPath):
            analysis = readXmlFile(analysisPath).getroot()
            runAliasThisAnalysis = getRunAlias(analysis)
            runAliasPrefix = re.search("^[a-z|A-Z|0-9]+", runAliasThisAnalysis).group()
            runTree = readXmlFile(runPath)
            run = runTree.getroot()
            for runObj in run.iter("RUN"):
                if runObj.tag == "RUN":
                    runObjPrefix = re.search("^[a-z|A-Z|0-9]+", runObj.get("alias")).group()
                    if not re.match(runAliasPrefix, runObjPrefix):
                        warnings.warn("Mismatch between analysis run ref %s and run alias %s in submission %s" % (runAliasThisAnalysis, run.get("alias"), thisSubmission))
                    runObj.set("alias", runAliasThisAnalysis)
            runTree.write(runPath)
        
if __name__ == '__main__':
    main()

    

