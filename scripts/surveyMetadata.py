#!/usr/bin/env python

import argparse
import common
import os
import re
import sys
import xml.etree.ElementTree as ET


class metadataObject( object ):
    " Records the usage of a given metadata item "

    def __init__( self, tag ):
        self.count = 0
        self.tag = tag
        self.attributes = dict()
        self.children = dict()

    def update(self, xmlObj, countRepeats=False):
        assert(xmlObj.tag == self.tag)
        self.count += 1
        for attribute in xmlObj.attrib.keys():
            if not self.attributes.has_key(attribute):
                self.attributes[attribute] = 0
            self.attributes[attribute] += 1
        childrenUpdated = dict()
        for child in xmlObj:
            childTag = child.tag
            if not self.children.has_key(childTag):
                self.children[childTag] = metadataObject(childTag)
            if countRepeats or not childrenUpdated.has_key(childTag):
                self.children[childTag].update(child, 
                                               countRepeats=countRepeats)
            childrenUpdated[childTag] = 1

    def dump(self, depth, showCounts=True, showAttributes=True):
        prefix = ""
        for ii in range(depth):
            prefix = prefix + "  "
        if showCounts:
            print "%s%s (%d)" % (prefix, self.tag, self.count),
        else:
            print "%s%s" % (prefix, self.tag),
        if showAttributes:
            print " [",
            for attribute in self.attributes.keys():
                if showCounts:
                    print " %s (%d)" % (attribute, 
                                         self.attributes[attribute]),
                else:
                    print " %s" % (attribute),
            print "]",
        print
        for childTag in self.children.keys():
            self.children[childTag].dump(depth+1, showCounts=showCounts,
                                         showAttributes=showAttributes)

        


def updateList(metadataTypes, thisMetadataXml):
    """Update the list according to what's observed in this XML object"""
    tag = thisMetadataXml.tag
    if not metadataTypes.has_key(tag):
        metadataTypes[tag] = metadataObject(tag)
    metadataTypes[tag].update(thisMetadataXml)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--counts", default=False, 
                        help="Show the counts as well as the structure")
    parser.add_argument("-a", "--attributes", default=False,
                        help="Show the attributes as well as the structure")
    args = parser.parse_args()

    metadataTypes = dict()
    for thisMetadataFile in sys.stdin:
        thisMetadataFile = thisMetadataFile.rstrip()
        thisMetadataXml = ET.parse(thisMetadataFile).getroot()
        updateList(metadataTypes, thisMetadataXml)
    for thisMetadataType in metadataTypes.keys():
        metadataTypes[thisMetadataType].dump(0, showCounts=args.counts,
                                             showAttributes=args.attributes)
    

if __name__ == '__main__':
    main( )
