#!/usr/bin/env python

import argparse, re, os, sys, csv, glob

def cmdParse():
    "parse command line and return arguments object"
    parser = argparse.ArgumentParser(description="""combine status files from metadata/*/metadata.status*.txt, add study""")
    parser.add_argument("metadataRootDirectory", type=str,
                        help="""root of directory with */metadata.status*.txt, where directory is the center name and file might be metadata.status.txt or  metadata.status.TARGET.txt""")
    parser.add_argument("allMetadataStatusTsv", type=str,
                        help="""commbined output file """)
    return parser.parse_args()

def writeRow(fh, row):
    fh.write('\t'.join([col if col != None else "" for col in  row])+'\n')

def getStatusFiles(metadataRootDirectory):
    pat = metadataRootDirectory + "/*/metadata.status*.txt"
    statusTsvs = glob.glob(pat)
    if len(statusTsvs) == 0:
        raise Exception("no status file found matching: " + pat)
    return statusTsvs

def decodeColumn(tsvRow, colName):
    val = tsvRow.get(colName)
    if (val == "None") or (val == ""):
        return None
    else:
        return val

def catMetadataTsv(statusTsv, statusTsvFh, allMetadataStatusfh):
    # metadata/CompleteGenomics/metadata.status.txt
    center = os.path.split(os.path.split(statusTsv)[0])[-1]
    for tsvRow in csv.DictReader(statusTsvFh, dialect=csv.excel_tab):
        # not all have SampleBarcode
        writeRow(allMetadataStatusfh, [center, decodeColumn(tsvRow, "Submission"), decodeColumn(tsvRow, "RunAccession"), decodeColumn(tsvRow,"Filename"), decodeColumn(tsvRow,"md5"), decodeColumn(tsvRow, "SampleBarcode"), decodeColumn(tsvRow,"ValidationStatus")])

def catMetadataTsvs(statusTsvs, allMetadataStatusfh):
    writeRow(allMetadataStatusfh, ("Center", "Submission", "RunAccession", "Filename", "md5", "SampleBarcode", "ValidationStatus"))
    for statusTsv in statusTsvs:
        with open(statusTsv) as statusTsvFh:
            catMetadataTsv(statusTsv, statusTsvFh, allMetadataStatusfh)

args = cmdParse()
statusTsvs = getStatusFiles(args.metadataRootDirectory)
with open(args.allMetadataStatusTsv, "w") as allMetadataStatusfh:
    catMetadataTsvs(statusTsvs, allMetadataStatusfh)
