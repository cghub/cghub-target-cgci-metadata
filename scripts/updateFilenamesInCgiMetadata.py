#!/usr/bin/env python

import argparse
import common
import csv
import os
import re
import xml.etree.ElementTree as ET

def readFilenamesAndChecksums(inputFile):
    filenamesAndChecksums = dict()
    fp = open(inputFile)
    for line in fp:
        tokens = line.rstrip().split()
        filenamesAndChecksums[tokens[0]] = tokens[1]
    return(filenamesAndChecksums)

def updateFilenamesAndChecksums(metadata, newFilenamesAndChecksums, debug=False):
    """From the DATA_BLOCK/FILES object, get the per-chrom filename from
        the first FILE object.  Derive the filename for a genome-wide BAM.
        If this BAM is in the list, then replace the FILES object with
        one for just this single file"""
    for myFiles in metadata.iter("FILES"):
        firstOldFilename = myFiles.find("FILE").get("filename")
        newFilename = re.sub("_(notMapped|chr(.){1,})\.bam$", ".bam", firstOldFilename)
        if debug:
            print "Investigating replacing", firstOldFilename, "with", newFilename
        if newFilenamesAndChecksums.has_key(newFilename):
            print "updating filenames in", metadata.get("accession")
            myFiles.clear()
            newFileObj = ET.SubElement(myFiles, "FILE")
            newFileObj.set("filename", newFilename)
            newFileObj.set("filetype", "bam")
            newFileObj.set("checksum_method", "MD5")
            newFileObj.set("checksum", newFilenamesAndChecksums[newFilename])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("metadataObjDir", type=str, 
                        help="Input directory of metadata objects")
    parser.add_argument('inputFile', type=str,
                        help="""A file with new filenames and checksums, 
                                possibly for some but not all of the metadata 
                                objects in the directory.  Assumptions:
                                the file is whitespace-delimited, has no
                                header row, and contains filenames in the 
                                first column and md5sums in the second""")
    parser.add_argument("-d", "--debug", type=str, default=False,
                        help="Print debugging info")
    args = parser.parse_args()

    newFilenamesAndChecksums = readFilenamesAndChecksums(args.inputFile)
    for metadataFile in os.listdir(args.metadataObjDir):
        metadataPathname = args.metadataObjDir + "/" + metadataFile
        if args.debug:
            print "working on", metadataFile
        metadataTree = ET.parse(metadataPathname)
        metadata = metadataTree.getroot()
        updateFilenamesAndChecksums(metadata, newFilenamesAndChecksums, args.debug)
        metadataTree.write(metadataPathname)


if __name__ == '__main__':
    main()

    
