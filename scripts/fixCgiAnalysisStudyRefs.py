#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET

def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("objectDir", type=str, 
                        default="perObject",
                        help="Directory with repackaged submissions")
    parser.add_argument('sraRunTable', type=str,
                        help="SRA Run Table for TARGET from dbGaP")
    parser.add_argument('projectAccessionsFile', type=str,
                        help="Project accession mappings")
    parser.add_argument("-d", "--debug", type=str, default=False, 
                        help="Display debugging info")
    args = parser.parse_args()

    sraRunTableBySraSample = common.readDictOfDicts(args.sraRunTable, 
                                                  keyColumn=16)
    projectAccessions = common.readDictOfDicts(args.projectAccessionsFile)
    analysisDir = args.objectDir + "/analysis"
    for analysisObj in os.listdir(analysisDir):
        if args.debug:
            print "working on analysis object", analysisObj 
        analysisPath = analysisDir + "/" + analysisObj
        analysisTree = readXmlFile(analysisPath)
        analysisXml = analysisTree.getroot()
        targetObj = analysisXml.find("TARGETS/TARGET")
        targetAccession = targetObj.get("accession")
        if args.debug:
            print "targetAccession", targetAccession
        if not sraRunTableBySraSample.has_key(targetAccession):
            if args.debug:
                print "targetAccession not in SRA run table"
        else:
            studyAccession = sraRunTableBySraSample[targetAccession]["SRA Study"]
            if args.debug:
                print "targetAccession", targetAccession, "with studyAccession", studyAccession
            studyRefObj = analysisXml.find("STUDY_REF")
            studyRefObj.set("refname", projectAccessions[studyAccession]["dbGaP_Accession"])
            studyRefObj.set("accession", studyAccession)
            if args.debug:
                print "setting study refname to", projectAccessions[studyAccession]["dbGaP_Accession"], "and studyAccession to", studyAccession
            analysisTree.write(analysisPath)
        

if __name__ == '__main__':
    main()
