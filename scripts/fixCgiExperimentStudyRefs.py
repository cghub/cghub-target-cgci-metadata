#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET

def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("objectDir", type=str, 
                        default="perObject",
                        help="Directory with repackaged submissions")
    parser.add_argument('sraRunTable', type=str,
                        help="SRA Run Table for TARGET from dbGaP")
    parser.add_argument('projectAccessionsFile', type=str,
                        help="Project accession mappings")
    parser.add_argument("-d", "--debug", type=str, default=False, 
                        help="Display debugging info")
    args = parser.parse_args()

    sraRunTableBySample = common.readDictOfDicts(args.sraRunTable, 
                                                 keyColumn=16)
    projectAccessions = common.readDictOfDicts(args.projectAccessionsFile)
    experimentDir = args.objectDir + "/experiment"
    for experimentObj in os.listdir(experimentDir):
        if args.debug:
            print "working on experiment object", experimentObj 
        experimentPath = experimentDir + "/" + experimentObj
        experimentTree = readXmlFile(experimentPath)
        experimentXml = experimentTree.getroot()
        sampleDescriptorObj = experimentXml.find("DESIGN/SAMPLE_DESCRIPTOR")
        sampleAccession = sampleDescriptorObj.get("accession")
        if args.debug:
            print "sampleAccession", sampleAccession
        if not sraRunTableBySample.has_key(sampleAccession):
            if args.debug:
                print "sampleAccession", sampleAccession, "not in SRA run table"
        else:
            studyAccession = sraRunTableBySample[sampleAccession]["SRA Study"]
            if args.debug:
                print "sampleAccession", sampleAccession, "with studyAccession", studyAccession
            studyRefObj = experimentXml.find("STUDY_REF")
            studyRefObj.set("refname", projectAccessions[studyAccession]["dbGaP_Accession"])
            studyRefObj.set("accession", studyAccession)
            if args.debug:
                print "setting study refname to", projectAccessions[studyAccession]["dbGaP_Accession"], "and studyAccession to", studyAccession
            experimentTree.write(experimentPath)
        

if __name__ == '__main__':
    main()
