#!/usr/bin/env python

import argparse, re, os, sys, csv
from collections import defaultdict, OrderedDict
from studies import studies

def cmdParse():
    "parse command line and return arguments object"
    parser = argparse.ArgumentParser(description="""Take various files that indicate what is know about TARGET data and metadata and correlate into a report.""")
    parser.add_argument("downloadedFiles", type=str,
                        help="""file containing paths of BAMs downloaded from NCBI. Generally, these will be ones that have not been submitted to CGHub, however it will report on ones that have been submitted but not purged.  This should include the directory path from NCBI, as it is searched for accessions.  Normally ./inputs/pending-download.files.  A line starting with `#' is considered a comment.""")
    parser.add_argument("downloadedImportStatusTsv", type=str,
                        help="""Status of the import of a subset of the files in downloadedFiles that have been processed""")
    parser.add_argument("cghubStatusTsv", type=str,
                        help="""cghub status information create by cghubMetadataCollect.py""")
    parser.add_argument("metadataStatusTsv", type=str,
                        help="""metadata status summary, create with: tawk 'NR==1||FNR>1' metadata/*/metadata.status.txt  >work/all.metadata.status.txt""")
    parser.add_argument("sraAccessionsTsv", type=str,
                        help="""SRA_Accessions.tab, MUST be process by sraAccessionsFilter.py, see documentation in the program for why""")
    parser.add_argument("sraAccessionsSkipListTsv", type=str,
                        help="""list of either SRR  or SRX accessions to ignore.  usually this is because they have exact (matching MD5) duplicates""")
    parser.add_argument("reportTsv", type=str,
                        help="""report written to this file""")
    return parser.parse_args()

emptyTuple = tuple()

srfGroups = {}

def writeRow(fh, row):
    fh.write('\t'.join([str(r) if r != None else "" for r in row])+'\n')

def noneIfEmpty(val):
    if len(val) == 0:
        return None
    else:
        return val

def emptyIfNone(val):
    if val == None:
        return ""
    else:
        return val

def firstOrEmpty(vals):
    "get first value from a list, or empty string if list is empty"
    if len(vals) == 0:
        return ""
    else:
        return vals[0]

def splitCommaList(strVals):
    """split into comma list, return empty list rather than one element if
    string is empty"""
    if len(strVals) == 0:
        return []
    else:
        return strVals.split(",")

def joinCommaList(vals):
    return ",".join(vals)

class excel_tab_noquoting(csv.excel_tab):
    """SRA_Accessions has columns like `"Institute of Vegetables and Flowers,
    Chinese Acad', so this just disables quote processing.  """
    quoting = csv.QUOTE_NONE

def studyToDesc(acc):
    "look up by all types of studies, default to returning acc"
    study = studies.getByAnyAcc(acc)
    if study == None:
        return acc
    else:
        return study.desc

class State(object):
    """Used to return descriptions of the statue of data and if it should be considered
    an error.
    """
    # categories
    ok = "ok"
    warn = "warn"
    error = "error"

    def __init__(self, category, desc, details=None):
        assert(desc != "" and (desc != None))
        self.category = category
        self.desc = desc
        self.details = details

    def isOk(self):
        return (self.category == self.ok)

    def isWarn(self):
        return (self.category == self.warn)

    def isError(self):
        return (self.category == self.error)

    def same(self, category, desc):
        "do category and description match?"
        return (self.category == category) and (self.desc == desc)

    def sameOk(self, desc):
        "category == ok and description match?"
        return (self.category == self.ok) and (self.desc == desc)

    def sameError(self, desc):
        "category == error and description match?"
        return (self.category == self.error) and (self.desc == desc)

class AccessionIndex(dict):
    "index for access to object mapping"
    def add(self, acc, obj):
        "add an entry, which must not exist"
        if acc in self:
            Exception("duplicate accession: " + acc)
        self[acc] = obj

    def get(self, *accSpecs):
        """get by either a single or list of accessions, or some combination. accs of none are not check."""
        for accSpec in accSpecs:
            if isinstance(accSpec, str):
                obj = dict.get(self, accSpec)
                if obj != None:
                    return obj
            elif accSpec != None:
                for acc in accSpec:
                    obj = dict.get(self, acc)
                    if obj != None:
                        return obj
        return None

    def getAllAccessions(self):
        return frozenset(self.iterkeys())

class Downloaded(object):
    "One set of of BAMs, SRFs, etc, from the downloadedFiles list, along with import status"

    def __init__(self, dirPath):
        # accessions are parsed from dirpath, but not all maybe present
        self.dirPath = dirPath
        self.numberFiles = 0
        self.exprAcc = self.runAcc = self.sampleType = self.sampleBarcode = self.study = None
        self.bamFiles = set()  # use set to avoid dups
        self.srfFiles = set()
        self.fastqFiles = set()
        self.allFiles = set()
        self.importStatus = None  # status from import
        self.inDownloadedList = False  # used to detect stray import status
        self.__parseDirPath(dirPath)

    def addFile(self, downloadedFile):
        "add a file path, skipping if unknown type or already exists"
        if downloadedFile.endswith(".bam"):
            self.bamFiles.add(downloadedFile)
        elif downloadedFile.endswith(".srf"):
            self.srfFiles.add(downloadedFile)
        elif downloadedFile.endswith(".gz") or downloadedFile.endswith(".fastq") or downloadedFile.endswith(".tar"):
            self.fastqFiles.add(downloadedFile)
        else:
            return
        self.numberFiles = self.numberFiles + 1
        self.allFiles.add(downloadedFile)

    def __parseDirPath(self, dirPath):
        for elem in dirPath.split("/"):
            self.__parseDirPathElement(elem)

    def __parseDirPathElement(self, elem):
        if elem.endswith("_sample") or elem.endswith("_sample_type"):
            # tumor_sample,normal_sample, etc.
            self.sampleType = elem
        elif elem.startswith("SRX"):
            self.exprAcc = elem
        elif elem.startswith("SRR"):
            # SRR563937_processed
            self.runAcc = elem.split("_")[0]
        elif re.match("^TARGET-[^-]+-[^-]+-[^-]+-[^-]+$", elem):
            # TARGET-10-PANDWE-03A-01D
            self.sampleBarcode = elem
        elif elem.startswith("phs") or elem.startswith("SRP"):
            self.study = elem

    def __str__(self):
        return " ".join([self.dirPath, str(self.numberFiles), str(self.exprAcc), str(self.runAcc), str(self.sampleType), str(self.sampleBarcode) ,
                         joinCommaList(self.bamFiles), joinCommaList(self.srfFiles), joinCommaList(self.fastqFiles)])

    def hasBamFiles(self):
        return len(self.bamFiles) > 0
    def hasSrfFiles(self):
        return len(self.srfFiles) > 0
    def hasFastqFiles(self):
        return len(self.fastqFiles) > 0
    def hasFiles(self):
        return self.hasBamFiles() or self.hasSrfFiles() or self.hasFastqFiles()

    def undefinedFields(self):
        "return list of names of fields that couldn't be defined, or None if all there"
        undefs = []
        if self.exprAcc == None:
            undefs.append("srx")
        if self.runAcc == None:
            undefs.append("srr")
        if self.sampleType == None:
            undefs.append("sampleType")
        if self.sampleBarcode == None:
            undefs.append("sampleBarcode")
        if not self.hasFiles():
            undefs.append("dataFiles")
        if len(undefs) == 0:
            return None
        else:
            return undefs

    def __checkSrfSet(self):
        if len(self.bamFiles) == 0:
            return State(State.ok, "srfSubmission")
        elif len(self.bamFiles) == 1:
            return State(State.ok, "srfBamSubmission")
        elif len(self.bamFiles) == 2:
            return State(State.error, "srfMultiBamSubmission")
        else:
            raise Exception("__checkSrfSet: bug:" + str(self))

    def __checkFastqSet(self):
        if len(self.bamFiles) == 0:
            return State(State.ok, "fastqSubmission")
        elif len(self.bamFiles) == 1:
            return State(State.ok, "fastqBamSubmission")
        elif len(self.bamFiles) == 2:
            return State(State.error, "fastqMultiBamSubmission")
        else:
            raise Exception("__checkFastqSet: bug:" + str(self))

    def __checkSingleBam(self):
        fileName = os.path.split(next(iter(self.bamFiles)))[1]
        if (fileName.find("_chr") >= 0) or (fileName.find("_notMapped") >= 0):
            return State(State.error, "partialChromSplitBam", fileName)
        elif fileName.find("dupsFlagged") >= 0:
            return State(State.ok, "dupsFlaggedBam")
        else:
            return State(State.ok, "singleBam")

    autosomes = frozenset(("chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20","chr21","chr22"))
    maleChroms = frozenset(("chrX","chrY"))
    femaleChroms = frozenset(("chrX",))
    mtChrom = frozenset(("chrM",))
    notMapped = frozenset(("notMapped",))

    maleExpected = frozenset(autosomes | maleChroms | mtChrom | notMapped)
    femaleExpected = frozenset(autosomes | femaleChroms | mtChrom | notMapped)
    allExpected = frozenset(maleExpected|femaleExpected)

    def __parseChromName(self, bamFile):
        fileName = os.path.split(bamFile)[1]
        m = re.match(".+_([^_.]+)\\.bam", fileName)
        if m == None:
            return fileName
        else:
            return m.group(1)
        
    def __parseChromNames(self):
        chroms = []
        for bamFile in self.bamFiles:
            chroms.append(self.__parseChromName(bamFile))
        return frozenset(chroms)

    def __checkMultiBam(self):
        chroms = self.__parseChromNames()
        if (chroms == self.maleExpected) or (chroms == self.femaleExpected):
             if self.importStatus != None:
                 return self.importStatus
             else:
                 return State(State.ok, "chromSplitBam")
        elif len(chroms & self.allExpected) > 0:
            return State(State.error, "partialChromSplitBam")
        else:
            return State(State.error, "multiBamUnexpectedNames")

    def checkDataFiles(self):
        """verify that downloaded have the right number of BAMs and other
        patterns, return Status object  """
        if len(self.srfFiles) > 0:
            return self.__checkSrfSet()
        elif len(self.fastqFiles) > 0:
            return self.__checkFastqSet()
        elif len(self.bamFiles) == 1:
            return self.__checkSingleBam()
        elif len(self.bamFiles) > 1:
            return self.__checkMultiBam()
        elif (self.importStatus != None) and self.importStatus.sameOk("released"):
            return self.importStatus  # data file not required for released
        else:
            return State(State.error, "noDataFiles")
                                  
class DownloadedTbl(list):
    "all downloaded file information parsed from downloadedFiles"
    def __init__(self, downloadedFiles, downloadedImportStatusTsv):
        self.byDirPath = {}
        self.runAccIdx = AccessionIndex()
        self.exprAccIdx = AccessionIndex()
        with open(downloadedFiles) as fh:
            self.__loadDownloadedList(fh)
        with open(downloadedImportStatusTsv) as fh:
            self.__loadDownloadedImportStatusTsv(fh)

    def __loadDownloadedList(self, fh):
        for line in fh:
            line = line.strip()
            if (len(line) > 0) and (line[0] != "#"):
                downloadedFile = self.__loadFilePath(line)
                downloadedFile.inDownloadedList = True

    def __loadDownloadedImportStatusTsv(self, fh):
        for tsvRow in csv.DictReader(fh, dialect=csv.excel_tab):
            self.__loadDownloadedImportStatus(tsvRow)

    def __loadDownloadedImportStatus(self, tsvRow):
        downloadedFile = self.__obtainForFilePath(tsvRow["path"])
        # file must be in download files unless it has been released.  This makes sure
        # path names are the same
        if not (downloadedFile.hasFiles() or (tsvRow["status"] == "released")):
            raise Exception("no download file found for unreleased download status, could file paths be different?: " + tsvRow["path"])
        downloadedFile.importStatus = self.__convertImportStatus(tsvRow["status"])

    def __loadFilePath(self, filePath):
        dirPath, fileName = os.path.split(filePath)
        if fileName.endswith('srf'):
            dirPath, srracc = os.path.split(dirPath)  
        downloadedFile = self.__obtain(dirPath)
        downloadedFile.addFile(fileName)
        return downloadedFile

    def __convertImportStatus(self, importStatus):
        if importStatus == "released":
            return State(State.ok, importStatus)
        #elif importStatus == "can_be_released":
        elif importStatus == "not_released":
            return State(State.warn, importStatus)
        else:
            #return State(State.ok, importStatus)
            return State(State.error, importStatus)

    def __obtainForFilePath(self, filePath):
        dirPath, fileName = os.path.split(filePath)
        if fileName.endswith('srf'):
            dirPath, srracc = os.path.split(dirPath)  
        return self.__obtain(dirPath)

    def __obtain(self, dirPath):
        downloaded = self.byDirPath.get(dirPath)
        if downloaded == None:
            downloaded = self.__create(dirPath)
        return downloaded

    def __create(self, dirPath):
        downloaded = Downloaded(dirPath)
        self.append(downloaded)
        self.byDirPath[dirPath] = downloaded
        if downloaded.runAcc != None:
            self.runAccIdx.add(downloaded.runAcc, downloaded)
        if downloaded.exprAcc != None:
            self.exprAccIdx.add(downloaded.exprAcc, downloaded)
        return downloaded

class CGHubStatus(object):
    "data about state in CGHub, which groups sets of experiment and run accessions"
    def __init__(self, tsvRow):
        self.analysisId = tsvRow["analysis_id"]
        self.study = tsvRow["study"]
        self.center = tsvRow["center"]
        self.state = State(State.ok if tsvRow["state"] == "live" else State.error, tsvRow["state"])
        self.sampleBarcode = noneIfEmpty(tsvRow["sample_barcode"])
        self.exprAccs = sorted(splitCommaList(tsvRow["expr_accessions"]))
        self.runAccs = sorted(splitCommaList(tsvRow["run_accessions"]))

    def __str__(self):
        return " ".join([self.analysisId, firstOrEmpty(self.runAccs), firstOrEmpty(self.exprAccs), self.state])

class CGHubStatusTbl(list):
    "data about status in CGHub for TARGET"
    def __init__(self, cghubStatusTsv):
        self.bySampleBarcode = defaultdict(list)  # allow multiple
        self.runAccIdx = AccessionIndex()
        self.exprAccIdx = AccessionIndex()
        with open(cghubStatusTsv) as tsvFh:
            for tsvRow in csv.DictReader(tsvFh, dialect=csv.excel_tab):
                self.__loadRow(tsvRow)

    def __loadRow(self, tsvRow):
        cghubStatus = CGHubStatus(tsvRow)
        self.append(cghubStatus)
        self.bySampleBarcode[cghubStatus.sampleBarcode].append(cghubStatus)
        for runAcc in cghubStatus.runAccs:
            self.runAccIdx.add(runAcc, cghubStatus)
        for exprAcc in cghubStatus.exprAccs:
            self.exprAccIdx.add(exprAcc, cghubStatus)

class MetadataStatus(object):
    """Metadata status from metadata analysis"""
    def __init__(self, tsvRow):
        self.center = tsvRow["GSC"]
        self.runAcc = noneIfEmpty(tsvRow["RunAccession"])
        self.gitId = noneIfEmpty(tsvRow["Submission"])
        self.status = self.__convertValidationStatus(tsvRow["ValidationStatus"])

    def __convertValidationStatus(self, validationStatus):
        if validationStatus == "OK":
            return State(State.ok, validationStatus)
        else:
            return State(State.error, validationStatus)

    def __str__(self):
        return " ".join(str(self.runAcc), str(self.gitId), str(self.validationStatus))

class MetadataStatusTbl(list):
    "data about status in CGHub for TARGET"
    def __init__(self, metadataStatusTsv):
        self.runAccIdx = AccessionIndex()
        with open(metadataStatusTsv) as tsvFh:
            for tsvRow in csv.DictReader(tsvFh, dialect=csv.excel_tab):
                self.__loadRow(tsvRow)

    def __loadRow(self, tsvRow):
        metadataStatus = MetadataStatus(tsvRow)
        self.append(metadataStatus)
        if metadataStatus.runAcc != None:
            self.runAccIdx.add(metadataStatus.runAcc, metadataStatus)

class SraGroup(object):
    """one grouping of run and experiment accessions from SRA_Accessions, this
    ends up being a CGHub submission.  This will have a SRA live run and experiment"""
    def __init__(self, runAcc, exprAcc, sampleAcc, runSubmissionAcc, sraStatus, study, center):
        self.runAcc = runAcc
        self.exprAcc = exprAcc
        self.sampleAcc = sampleAcc
        self.runSubmissionAcc = runSubmissionAcc
        self.sraStatus = sraStatus
        self.study = study
        self.center = center
        self.suppressedRunAccs = []  # sorted
        self.suppressedExprAccs = []

    def __str__(self):
        return " ".join([self.runAcc, self.exprAcc, self.study, self.center, joinCommaList(self.suppressedRunAccs), joinCommaList(self.suppressedExprAccs)])

    def __hash__(self):
        return hash(self.runAcc) + hash(self.exprAcc) + hash(self.study) + hash(self.center)

    def __eq__(self, other):
        return (isinstance(other, SraGroup) and 
                (self.runAcc == other.runAcc) and
                (self.exprAcc == other.exprAcc) and
                (self.study == other.study) and
                (self.center == other.center) and
                (self.suppressedRunAccs == other.suppressedRunAccs) and
                (self.suppressedExprAccs == other.suppressedExprAccs))

    def __ne__(self, other):
        return not (self == other)

class SraGrouper(object):
    """ build SraGroup objects from SRA_Accessions.tab"""
    def __init__(self, sraAccessionsTsv):
        self.sraGroups = []
        self.liveExprs = {}
        self.liveRuns = {}
        self.suppressedExprs = {}
        self.suppressedRuns = {}
        self.exprAccToRunAccs = defaultdict(list) 
        self.replacingExprs = defaultdict(list)  # map of acc to those it replaces
        self.replacingRuns = defaultdict(list)
        self.doneRows = set()  # rows of any type we have completed
        self.__readSraAccessions(sraAccessionsTsv)

    def __addUniqRow(self, accMap, tsvRow):
        "add row to specified map, error if it already exists"
        acc = tsvRow["Accession"]
        if acc in accMap:
            raise Exception("unexpected duplicated accession: " + acc)
        accMap[acc] = tsvRow
    
    def __flagDone(self, tsvRow):
        self.doneRows.add(id(tsvRow))

    def __isDone(self, tsvRow):
        return id(tsvRow) in self.doneRows

    def __checkNotDone(self, tsvRow):
        if id(tsvRow) in self.doneRows:
            raise Exception("tsvRow has already been processes: " + tsvRow["Accession"])

    def __checkAndFlagDone(self, tsvRow):
        self.__checkNotDone(tsvRow)
        self.__flagDone(tsvRow)

    def __doctorRow(self, fieldnames, tsvRow):
        "set columns with are empty or `-' to None"
        for col in fieldnames:
            cell = tsvRow[col]
            if cell in ("", "-"):
                tsvRow[col] = None

    def __readSraAccessions(self, sraAccessionsTsv):
        "read all rows into per-accession maps"
        # n.b. file from NCBI has ms-dos newlines
        # n.b. do *not* filter by study, as some suppressed don't have a study
        #      sraAccessionsFilter handles this.
        with open(sraAccessionsTsv, "U") as tsvFh:
            tsv = csv.DictReader(tsvFh, dialect=excel_tab_noquoting)
            for tsvRow in tsv:
                self.__doctorRow(tsv.fieldnames, tsvRow)
                self.__processRow(tsvRow)

    def __processRow(self, tsvRow):
        if tsvRow["Type"] == "RUN":
            self.__processRunRow(tsvRow)
        elif tsvRow["Type"] == "EXPERIMENT":
            self.__processExprRow(tsvRow)

    def __processRunRow(self, tsvRow):
        if tsvRow["Status"] == "live":
            self.__addUniqRow(self.liveRuns, tsvRow)
        #TODO: allow this back? want to avoid finding suppressed files
        else:
             return
        #    self.__addUniqRow(self.suppressedRuns, tsvRow)
        #    self.replacingRuns[tsvRow["ReplacedBy"]].append(tsvRow["Accession"])
        # map experiments to this run
        if tsvRow["Experiment"] != None:
            self.exprAccToRunAccs[tsvRow["Experiment"]].append(tsvRow["Accession"])

    def __processExprRow(self, tsvRow):
        if tsvRow["Status"] == "live":
            self.__addUniqRow(self.liveExprs, tsvRow)
        #TODO: allow this back? want to avoid finding suppressed files
        #else:
        #    self.__addUniqRow(self.suppressedExprs, tsvRow)
        #    self.replacingExprs[tsvRow["ReplacedBy"]].append(tsvRow["Accession"])
        
    def buildGroups(self):
        "build a list of SraGroup objects from data that was parsed"

        #groups = [self.__groupFromLiveRun(runRow) for runRow in self.liveRuns.itervalues()]
        #ensure sorted keys (which are expected to be SRA accessions of the regex SR[R,A,S,Z,P,X]\d+
        groups = [self.__groupFromLiveRun(self.liveRuns[runRowKey]) for runRowKey in sorted(self.liveRuns.keys(),cmp=lambda x,y: cmp(int(x[3:]),int(y[3:])))]
        return groups

    def __groupFromLiveRun(self, runRow):
        exprRow = self.liveExprs[runRow["Experiment"]]
        self.__checkAndFlagDone(runRow)  # run should only be processed once
        self.__flagDone(exprRow) # expr maybe processed multiple times
        # create combined status if expr and run not the same
        sraStatus = exprRow["Status"] if exprRow["Status"] == runRow["Status"] else exprRow["Status"] + "+" + runRow["Status"]
        sraGroup = SraGroup(runRow["Accession"], exprRow["Accession"], runRow["Sample"], runRow["Submission"], sraStatus, runRow["Study"], runRow["Center"])
        self.__bindSuppressedRuns(sraGroup, runRow)
        self.__bindSuppressedExprs(sraGroup, exprRow)
        sraGroup.suppressedRunAccs.sort()
        sraGroup.suppressedExprAccs.sort()
        return sraGroup

    def __bindSuppressedRuns(self, sraGroup, runRow):
        for supRunAcc in self.replacingRuns.get(sraGroup.runAcc, emptyTuple):
            supRunRow = self.suppressedRuns[supRunAcc]
            self.__checkAndFlagDone(supRunRow)
            sraGroup.suppressedRunAccs.append(supRunRow["Accession"])
        
    def __bindSuppressedExprs(self, sraGroup, exprRow):
        for supExprAcc in self.replacingExprs.get(sraGroup.exprAcc, emptyTuple):
            supExprRow = self.suppressedExprs[supExprAcc]
            self.__checkAndFlagDone(supExprRow)
            sraGroup.suppressedExprAccs.append(supExprRow["Accession"])
            self.__bindSuppressedExprRuns(sraGroup, supExprAcc)

    def __bindSuppressedExprRuns(self, sraGroup, supExprAcc):
        "make sure all runs associate with a suppressed experiment are included "
        for supRunAcc in self.exprAccToRunAccs.get(supExprAcc, emptyTuple):
            supRunRow = self.suppressedRuns[supRunAcc]
            if not self.__isDone(supRunRow):
                self.__checkAndFlagDone(supRunRow)
                sraGroup.suppressedRunAccs.append(supRunAcc)

    def __checkForStrays(self):
        "check for rows that have not be processed"
        unprocessed = [tsvRow["Accession"] for tsvRow in self.liveExprs.itervalues() if not self.__isDone(tsvRow)] \
            + [tsvRow["Accession"] for tsvRow in self.suppressedExprs.itervalues() if not self.__isDone(tsvRow)] \
            + [tsvRow["Accession"] for tsvRow in self.suppressedRuns.itervalues() if not self.__isDone(tsvRow)]
        if len(unprocessed) > 0:
            raise Exception("ungrouped accessions: " + joinCommaList(unprocessed))

class SraGroupTbl(list):
    "data about state in SRA for TARGET/CGCI"
    def __init__(self, sraGroups, sraSkipList):
        self.runAccIdx = AccessionIndex()
        self.exprAccIdx = AccessionIndex()
        for sraGroup in sraGroups:
            self.__addSraGroup(sraGroup, sraSkipList)

    def __addSraGroup(self, sraGroup, sraSkipList):
        #CW: dont want to track anything thats in the skip list
        if sraGroup.exprAcc in sraSkipList or sraGroup.runAcc in sraSkipList:
            return
        self.append(sraGroup)
        self.runAccIdx.add(sraGroup.runAcc, sraGroup)
        for runAcc in sraGroup.suppressedRunAccs:
            self.runAccIdx.add(runAcc, sraGroup)
        self.exprAccIdx.add(sraGroup.exprAcc, sraGroup)
        for exprAcc in sraGroup.suppressedExprAccs:
            self.exprAccIdx.add(exprAcc, sraGroup)

class Grouping(object):
    """ Grouping of all data for a given BAM (or set of split BAMs) on run
    accession.  Should correspond to either a cghub submission or dangling
    data.  These objects are were all information is linked from the various
    sources."""

    def __init__(self, sraGroup, downloaded, metadataStatus, cghubStatus):
        """Generate report for one item.  Probs is a set of strings to report.  Any of
        the status objects maybe none if missing"""
        self.sraGroup, self.downloaded, self.metadataStatus, self.cghubStatus = sraGroup, downloaded, metadataStatus, cghubStatus
        #track the multi-SRR, multi-file SRFs
        self.sraGroups = OrderedDict()       
        self.downloadeds = OrderedDict()        
        self.metaStatuses = OrderedDict()        
        self.cghubStatuses = OrderedDict()

        self.sraGroups[sraGroup]=1
        self.downloadeds[downloaded]=1
        self.metaStatuses[metadataStatus]=1
        self.cghubStatuses[cghubStatus]=1
        #self.runFiles = set()         
        #self.runAccs.add(sraGroup.runAcc)
        #for file in downloaded.allFiles:
        #    self.runFiles.add(file)
        #self.runFiles |= downloaded.allFiles
    
    def add(self, sraGroup, downloaded, metadataStatus, cghubStatus):
        #self.sraGroups.add(sraGroup]=1
        #self.downloadeds.add(downloaded)
        #self.metaStatuses.add(metadataStatus)
        #self.cghubStatuses.add(cghubStatus)
        self.sraGroups[sraGroup]=1
        self.downloadeds[downloaded]=1
        self.metaStatuses[metadataStatus]=1
        self.cghubStatuses[cghubStatus]=1

    def getSraStatus(self):
        if len(self.sraGroups) > 0:
            return_str = ";".join([sr.sraStatus for sr in self.sraGroups if sr])
            if len(return_str) == 0:
                return "unknown"
            return return_str
        #if self.sraGroup != None:
        #    return self.sraGroup.sraStatus
        else:
            return "unknown"

    def __get_list(self, attr_name, set_, set_func, return_set=False):
        list_ = []
        temp_ = self.__dict__[attr_name]
        for elem in set_:
            self.__setattr__(attr_name,elem)
            list_.append(set_func())
        self.__setattr__(attr_name,temp_)
        if return_set:
             super_set = set()
             for x in list_:
                 if x:
                     super_set |= set(x)
             return sorted(super_set)
        return ";".join([x for x in list_ if x])

    def getRunAcc(self):
        return self.__get_list("sraGroup", self.sraGroups, self.__getRunAcc)
        
    def __getRunAcc(self):
        """get run accession, trying various sources.  cghubStatus is tried
        last, since it can have multiple accessions, so the first is returned"""
        if (self.sraGroup != None) and (self.sraGroup.runAcc != None):
            return self.sraGroup.runAcc
        elif (self.downloaded != None) and  (self.downloaded.runAcc != None):
            return self.downloaded.runAcc
        elif (self.cghubStatus != None) and (len(self.cghubStatus.runAccs) > 0):
            return self.cghubStatus.runAccs[0]
        else:
            return None

    def getExprAcc(self):
        """get the experiment accession, trying various sources"""
        if (self.sraGroup != None) and (self.sraGroup.exprAcc != None):
            return self.sraGroup.exprAcc
        elif (self.downloaded != None) and  (self.downloaded.exprAcc != None):
            return self.downloaded.exprAcc
        elif (self.cghubStatus != None) and (len(self.cghubStatus.exprAccs) > 0):
            return self.cghubStatus.exprAccs[0]
        else:
            return None

    def getSampleAcc(self):
        """get the sample accession"""
        if self.sraGroup != None:
            return self.sraGroup.sampleAcc
        else:
            return None

    def getSraRunSubmissionAcc(self):
        """get the SRA submission accession, trying various sources"""
        if self.sraGroup != None:
            return self.sraGroup.runSubmissionAcc
        else:
            return None

    def getSampleBarCode(self):
        if (self.downloaded != None) and  (self.downloaded.sampleBarcode != None):
            return self.downloaded.sampleBarcode
        elif (self.cghubStatus != None) and (self.cghubStatus.sampleBarcode != None):
            return self.cghubStatus.sampleBarcode
        else:
            return None

    def getCenter(self):
        """get center name"""
        if self.sraGroup != None:
            return emptyIfNone(self.sraGroup.center)
        elif self.cghubStatus != None:
            return emptyIfNone(self.cghubStatus.center)
        elif self.metadataStatus != None:
            return emptyIfNone(self.metadataStatus.center)
        else:
            return None

    def getStudyDesc(self):
        """get study description"""
        # try download last, as it's usually TARGET parent study
        if self.sraGroup != None:
            return studyToDesc(self.sraGroup.study)
        elif self.cghubStatus != None:
            return studyToDesc(self.cghubStatus.study)
        elif self.downloaded != None:
            return studyToDesc(self.downloaded.study)
        else:
            return None

    #def getMetadataState(self):
    #    return self.__get_list("cghubStatus", self.cghubStatuses, self.__getMetadataState)

    def getMetadataState(self):
        if (self.cghubStatus != None) and self.cghubStatus.state.sameOk("live"):
            return self.cghubStatus.state
        elif self.metadataStatus != None:
            return self.metadataStatus.status
        else:
            return State(State.error, "noMetadata")
    
    #def getCGHubState(self):
    #    return self.__get_list("cghubStatus", self.cghubStatuses, self.__getCGHubState)

    def getCGHubState(self):
        """determine state of data at cghub, this can be publication state if
        submitted, download from NCBI and pending, has had a submission error,
        etc.  This also detects multiple submissions on an experiment at SRA
        not reflected in CGHub"""
        if self.cghubStatus != None:
            return self.cghubStatus.state
        else:
            return State(State.ok, "notLoaded")
    
    #def getDownloadedState(self):
    #    return self.__get_list("downloaded", self.downloadeds, self.__getDownloadedState)

    def getDownloadedState(self):
        if (self.downloaded != None):
            downloadedState = self.downloaded.checkDataFiles()
            if downloadedState.isOk() and (self.cghubStatus == "live"):
                downloadedState = State(State.warn, "liveNeedsDeleted")
        else:
            downloadedState = State(State.ok, "absent")
        return downloadedState
    
    def getDownloadedDir(self):
        return self.__get_list("downloaded", self.downloadeds, self.__getDownloadedDir)

    def __getDownloadedDir(self):
        return self.downloaded.dirPath if self.downloaded != None else ""
    
    def getDownloadedFilenames(self):
        return self.__get_list("downloaded", self.downloadeds, self.__getDownloadedFilenames)

    def __getDownloadedFilenames(self):
        return ",".join(self.downloaded.bamFiles.union(self.downloaded.srfFiles.union(self.downloaded.fastqFiles))) if self.downloaded != None else ""
    
    def getAllRunAccs(self):
        return self.__get_list("sraGroup", self.sraGroups, self.__getAllRunAccs, return_set=True)

    def __getAllRunAccs(self):
        accs = set()
        if self.sraGroup != None:
            accs.add(self.sraGroup.runAcc)
            accs |= set(self.sraGroup.suppressedRunAccs)
        if self.cghubStatus != None:
            accs |= set(self.cghubStatus.runAccs)
        return sorted(accs)

    def getAllExprAccs(self):
        accs = set()
        if self.sraGroup != None:
            accs.add(self.sraGroup.exprAcc)
            accs |= set(self.sraGroup.suppressedExprAccs)
        if self.cghubStatus != None:
            accs |= set(self.cghubStatus.exprAccs)
        return sorted(accs)

    def getMetadataGitId(self):
        return self.metadataStatus.gitId if self.metadataStatus != None else None

    def getAnalysisId(self):
        return self.cghubStatus.analysisId if self.cghubStatus != None else None

    def __str__(self):
        return " ".join([emptyIfNone(self.getRunAcc()), emptyIfNone(self.getExprAcc()), emptyIfNone(self.getSampleBarCode()),])

class Groupings(list):
    """All groups, either by accession grouping or stray data we can't group """
    def __init__(self, sraGroupTbl, downloadedTbl, metadataStatusTbl, cghubStatusTbl):
        self.sraGroupTbl, self.downloadedTbl, self.metadataStatusTbl, self.cghubStatusTbl = sraGroupTbl, downloadedTbl, metadataStatusTbl, cghubStatusTbl
        self.done = set()  # all status objects of all types add to groups record here
        #track when we associated 2 different run objects with the same downloaded file
        self.srr2file_tracker = {}
        #SRFs are tricky because they're really part of one submission (multiple lanes/runs of the same overall experiment) but split into separate files AND SRR accessions, join them by tracking their SRXs
        self.srx2srf_grouping = {}
        self.liveExprRunMap = defaultdict(list)
        self.__groupBySraGroups()
        self.__checkForStrayDownloaded()
        self.__checkForStrayMetadataStatus()
        self.__checkForStrayCgHubStatus()
        self.__buildLiveExprRunMap()
        self.sort(key=lambda g: (g.getRunAcc(), g.getExprAcc(), g.getSampleBarCode(), g.getCenter()))

    def __markGrouped(self, *objs):
        "flag status objects as grouped, if not None"
        for obj in objs:
            if obj != None:
                self.done.add(obj)

    def __isGrouped(self, obj):
        "has status object been grouped?"
        return (obj != None) and (obj in self.done)

    def __addGrouping(self, sraGroup, downloaded, metadataStatus, cghubStatus, is_srf=False):
        if is_srf and sraGroup.exprAcc in self.srx2srf_grouping:
            grouping = self.srx2srf_grouping[sraGroup.exprAcc]
            grouping.add(sraGroup, downloaded, metadataStatus, cghubStatus)
        else:
            grouping = Grouping(sraGroup, downloaded, metadataStatus, cghubStatus)
            self.append(grouping)
            if is_srf:
                self.srx2srf_grouping[sraGroup.exprAcc]=grouping
        self.__markGrouped(sraGroup, downloaded, metadataStatus, cghubStatus)

    def __getUngroupMetadataStatus(self, runAccSpecs):
        "look up metadata status, returning None if found one is already grouped"
        metadataStatus = self.metadataStatusTbl.runAccIdx.get(runAccSpecs)
        return metadataStatus if not self.__isGrouped(metadataStatus) else None

    def __getUngroupDownloaded(self, runAccSpecs, exprAccSpecs):
        "look up downloaded, returning None if found one is already grouped"
        downloaded = self.downloadedTbl.runAccIdx.get(runAccSpecs)
        if downloaded == None:
            downloaded = self.downloadedTbl.exprAccIdx.get(exprAccSpecs)
        return downloaded if not self.__isGrouped(downloaded) else None

    def __getUngroupCghubStatus(self, runAccSpecs, exprAccSpecs):
        "look up cghubStatus, returning None if found one is already grouped"
        cghubStatus = self.cghubStatusTbl.runAccIdx.get(runAccSpecs)
        if cghubStatus == None:
            cghubStatus = self.cghubStatusTbl.exprAccIdx.get(exprAccSpecs)
        return cghubStatus if not self.__isGrouped(cghubStatus) else None

    def __groupBySraGroups(self):
        for sraGroup in self.sraGroupTbl:
            self.__groupBySraGroup(sraGroup)

    #CW: this is where a number of bugs potentially arise:
    #1) tracking suppressed Run and Experiment Accessions is a bad idea because we don't want to count a suppressed, older version of the experiment as proof we have it (which was what was happening)
    #2) tracking groups of files (fastqs and SRFs) which should be grouped into one tracking "unit" (single line in output)
    def __groupBySraGroup(self, sraGroup):
        "group by sraGroup information.  This should be done first"
        # these may come back None
        downloaded = self.downloadedTbl.runAccIdx.get(sraGroup.runAcc)
        if downloaded == None:
            #TODO: do we really want to include suppressed accessions in our retrieval?
            #the answer is: No we want to ignore suppressed accessions, otherwise we'll wind up counting them as here when they should not be
            downloaded = self.downloadedTbl.exprAccIdx.get(sraGroup.exprAcc)
            #make sure that for non-CompleteGenomics multi-file grouping we keep the running tab of files to # of associated SRRs equal (or less SRRs), but not for ones that have FASTQs or SRFs (so BAMs only)
            if sraGroup.center != 'CompleteGenomics' and downloaded and downloaded.dirPath in self.srr2file_tracker and downloaded.numberFiles <= len(self.srr2file_tracker[downloaded.dirPath]) and len(downloaded.fastqFiles) == 0 and len(downloaded.srfFiles) == 0:
                #TODO: compare previous and current run ACCs and try to only keep the older one (difficult since the newer one might have already been reported)
                #UPDATED: ensured a sort on the groups on SRR run, should mean the earlier one appears first
                downloaded = None

        is_srf = False 
        if downloaded:
            if downloaded.dirPath not in self.srr2file_tracker:
                self.srr2file_tracker[downloaded.dirPath] = set()
            self.srr2file_tracker[downloaded.dirPath].add(sraGroup.runAcc)
            if len(downloaded.srfFiles) > 0:
                is_srf = True
            #track SRFs 
            #if len(downloaded.srfFiles) > 0:
            #   srf_present = True
            #   if sraGroup.exprAcc not in self.srx2srf_tracker:
            #       self.srx2srf_tracker[sraGroup.exprAcc] = set()
            #       srf_present = False
            #   self.srx2srf_tracker[sraGroup.exprAcc].add([downloaded.dirPath,sraGroup.runAcc])

        metadataStatus = self.metadataStatusTbl.runAccIdx.get(sraGroup.runAcc)
        cghubStatus = self.cghubStatusTbl.runAccIdx.get(sraGroup.runAcc)
        self.__addGrouping(sraGroup, downloaded, metadataStatus, cghubStatus, is_srf)

    def __checkForStrayDownloaded(self):
        for downloaded in self.downloadedTbl:
            if not self.__isGrouped(downloaded):
                self.__groupStrayDownload(downloaded)

    def __groupStrayDownload(self, downloaded):
        metadataStatus = self.__getUngroupMetadataStatus(downloaded.runAcc)
        cghubStatus = self.__getUngroupCghubStatus(downloaded.runAcc, downloaded.exprAcc)
        self.__addGrouping(None, downloaded, metadataStatus, cghubStatus)

    def __checkForStrayMetadataStatus(self):
        for metadataStatus in self.metadataStatusTbl:
            if not self.__isGrouped(metadataStatus):
                self.__groupStrayMetadataStatus(metadataStatus)

    def __groupStrayMetadataStatus(self, metadataStatus):
        downloaded = self.__getUngroupDownloaded(metadataStatus.runAcc, None)
        cghubStatus = self.__getUngroupCghubStatus(metadataStatus.runAcc, None)
        self.__addGrouping(None, downloaded, metadataStatus, cghubStatus)
            
    def __checkForStrayCgHubStatus(self):
        for cghubStatus in self.cghubStatusTbl:
            if not self.__isGrouped(cghubStatus):
                self.__groupStrayCgHubStatus(cghubStatus)

    def __groupStrayCgHubStatus(self, cghubStatus):
        downloaded = self.__getUngroupDownloaded(cghubStatus.runAccs, cghubStatus.exprAccs)
        metadataStatus = self.__getUngroupMetadataStatus(cghubStatus.runAccs)
        self.__addGrouping(None, downloaded, metadataStatus, cghubStatus)

    def __buildLiveExprRunMap(self):
        for sraGroup in self.sraGroupTbl:
            self.liveExprRunMap[sraGroup.exprAcc].append(sraGroup.runAcc)




class Reporter(object):
    "generate report from grouped data"

    def __init__(self, groupings):
        self.groupings = groupings
    #states are 5,6 10,11, 14,15
    reportHeader = ("runAcc", "exprAcc", "sampleAcc", "runSubmissionAcc", "sraStatus", "sraInfo", "study", "center", "sampleBarcode", "metadataState", "metadataDesc", "metadataGitId", "downloadedState", "downloadedDesc", "cghubState", "cghubDesc", "analysisId", "allRunAccs", "allExprAccs", "downloadedDir","filenames")

    def __getSraInfo(self, group):
        exprAcc = group.getExprAcc()
        if exprAcc == None:
            return None
        elif len(self.groupings.liveExprRunMap[exprAcc]) > 1:
            return "multiLiveExpr"
        else:
            return None

    def report(self, fh):
        writeRow(fh, self.reportHeader)
        for group in groupings:
            self.__reportGroup(fh, group)

    def __reportGroup(self, fh, group):
        metadataState = group.getMetadataState()
        cghubState = group.getCGHubState()
        downloadedState = group.getDownloadedState()
        
        writeRow(fh, (group.getRunAcc(),
                      group.getExprAcc(),
                      group.getSampleAcc(),
                      group.getSraRunSubmissionAcc(),
                      group.getSraStatus(),
                      self.__getSraInfo(group),
                      group.getStudyDesc(),
                      group.getCenter(),
                      group.getSampleBarCode(),
                      metadataState.category, metadataState.desc, group.getMetadataGitId(),
                      downloadedState.category, downloadedState.desc,
                      cghubState.category, cghubState.desc, group.getAnalysisId(),
                      joinCommaList(group.getAllRunAccs()), joinCommaList(group.getAllExprAccs()),
                      group.getDownloadedDir(),
                      group.getDownloadedFilenames()))


def load_skip_list(skipListTSV):
    with open(skipListTSV,"r") as f:
        skipList = {x.rstrip(): 1 for x in f.readlines()}
    return skipList
    

args = cmdParse()
sraSkipList = load_skip_list(args.sraAccessionsSkipListTsv)
sraGroupTbl = SraGroupTbl(SraGrouper(args.sraAccessionsTsv).buildGroups(),sraSkipList)
downloadedTbl = DownloadedTbl(args.downloadedFiles, args.downloadedImportStatusTsv)
cghubStatusTbl = CGHubStatusTbl(args.cghubStatusTsv)
metadataStatusTbl =  MetadataStatusTbl(args.metadataStatusTsv)

groupings = Groupings(sraGroupTbl, downloadedTbl, metadataStatusTbl, cghubStatusTbl)
reporter = Reporter(groupings)

with open(args.reportTsv, "w") as fh:
    reporter.report(fh)
