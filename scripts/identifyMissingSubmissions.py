#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('sraRunTable', type=str,
                        help="Pathname for the SRA run table")
    parser.add_argument('-d', '--debug', type=str, default=False,
                        help="Display debugging info?")
    args = parser.parse_args()
    
    sraRunTable = common.readDictOfDicts(args.sraRunTable)
    identifyMissingSubmissions(sraRunTable, debug=args.debug)



def filenameListFromRunObjects(manifestFile):
    """Read the manifest file and return a hash that maps run objects to a 
    list of corresponding filenames"""
    filenamesByRun = dict()
    fp = open(manifestFile)
    for row in fp:
        tokens = re.split(", ", row.rstrip())
        filename = tokens[0]
        runObject = tokens[2]
        if not filenamesByRun.has_key(runObject):
            filenamesByRun[runObject] = list()
        filenamesByRun[runObject].append(filename)
    return(filenamesByRun)
        

def identifyMissingSubmissions(sraRunTable, debug=False):
    """Given the submissions that are both in the run table and the manifest,
    determine which ones do not correspond to a submission at CGHub, and 
    generate a list of files for those objects"""
    print "Run\tSample\tStudy\tAssay\tCenter\tMBytes\tStatus"
    for runAccession in sraRunTable.keys():
        print "%s\t%s\t%s\t%s\t%s\t%s" % (runAccession,
                                          sraRunTable[runAccession]["Sample Name"], 
                                          sraRunTable[runAccession]["study_name"],
                                          sraRunTable[runAccession]["Assay Type"], 
                                          sraRunTable[runAccession]["Center Name"],
                                          sraRunTable[runAccession]["MBytes"]),
        if not sampleAtCghub(sraRunTable[runAccession]):
            print "Absent"
        else:
            print "Present"
            

def sampleAtCghub(sraRunTableEntry):
    """Given an entry from the SRA run table, determine if there is a 
    corresponding sample at CGHub.  Search cgquery on the legacy sample ID, get the XML, and
    see if the XML contains a RUN object with the target accession"""
    xmlPathname = sraRunTableEntry["Run"] + ".test.xml"
    cmd = "cgquery -a \"legacy_sample_id=%s\" -o %s" % (sraRunTableEntry["Sample Name"], 
                                                        xmlPathname)
    subprocess.check_output(cmd, shell=True)
    if os.path.exists(xmlPathname):
        xmlData=ET.parse(xmlPathname).getroot()
        for runSetObj in xmlData.iter("RUN_SET"):
            for runObj in xmlData.iter("RUN"):
                if "accession" in runObj.keys():
                    if runObj.get("accession") == sraRunTableEntry["Run"]:
                        os.remove(xmlPathname)
                        return(True)
        os.remove(xmlPathname)
    return(False)

    

if __name__ == '__main__':
    main()

