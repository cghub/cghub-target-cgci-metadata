#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)

def fixStudyRef(experimentXml, refcenter, allProjectAccessions):
    """In the STUDY_REF object, specify the appropriate refcenter, and 
    fill in the project accession if needed"""
    for studyRef in experimentXml.iter("STUDY_REF"):
        studyRef.set("refcenter", refcenter)
        if studyRef.get("refname") is None:
            projectAccession = studyRef.get("accession")
            if allProjectAccessions.has_key(projectAccession):
                refname = allProjectAccessions[projectAccession]["dbGaP_Accession"]
                studyRef.set("refname", refname) 


def fixExperimentRefRefname(runXml):
    """If there is no refname, add one frm the submitter ID if available"""
    eRef = runXml.find("EXPERIMENT_REF")
    if eRef.get("refname") is None:
        subId = eRef.find("IDENTIFIERS/SUBMITTER_ID")
        if subId is not None:
            eRef.set("refname", subId.text)
    if eRef.get("refcenter") is None:
        eRef.set("refcenter", runXml.get("center_name"))


def fixAnalysisRunXref(analysisXml, runXml):
    """Fix up the cross-referencing information between analysis and run."""
    # First, gather up a dict of the run objects, indexed by accession.
    allRunObjs = dict()
    for runObj in runXml.iter("RUN"):
        alias = runObj.get("alias")
        readGroupLabel = runObj.find("EXPERIMENT_REF").get("refname")
        if readGroupLabel is None:
            readGroupLabel = runObj.find("EXPERIMENT_REF/IDENTIFIERS/SUBMITTER_ID").text
            eRef = runObj.find("EXPERIMENT_REF")
            eRef.set("refname", readGroupLabel)
            eRef.set("refcenter", runObj.get("center_name"))
        allRunObjs[alias] = {"center_name": runObj.get("center_name"),
                             "accession": runObj.get("accession"),
                             "read_group_label": readGroupLabel,
                             "alias": alias, "included": 0 }
    # Next, find the run refs in the analysis object.  For any that have
    # the same accession as one of the runs catalogued, fill in the missing
    # info from the dictionary
    runLabels = analysisXml.find("ANALYSIS/ANALYSIS_TYPE/REFERENCE_ALIGNMENT/RUN_LABELS")
    for runReference in runLabels:
        refname = runReference.get("refname")
        assert(allRunObjs.has_key(refname))
        runObj = allRunObjs[refname]
        runReference.set("refcenter", runObj["center_name"])
        runObj["included"] = 1
    # Now add analysis references to any run objects not referenced
    for runObj in allRunObjs.values():
        if runObj["included"] == 0:
            newRunRef = ET.Element("RUN", 
                                   attrib={"accession": runObj["accession"],
                                           "read_group_label": runObj["read_group_label"],
                                           "refname": runObj["alias"],
                                           "refcenter": runObj["center_name"]})
            runLabels.append(newRunRef)
                                           

def fixRunExperimentXref(runXml, experimentXml):
    """Fix up the cross-referencing information between run and experiment."""
    experiment = experimentXml.find("EXPERIMENT")
    for experimentRef in runXml.iterfind("RUN/EXPERIMENT_REF"):
        print "changing experiment alias for run from", experimentRef.get("refname"), "to", experiment.get("alias")
        experimentRef.set("refname", experiment.get("alias"))
        experimentRef.set("refcenter", experiment.get("center_name"))
        


def fixSampleDescriptor(root, sraRunTable, refcenter="NCI_TARGET"):
    """Ensure that the sample descriptor has a refname and refcenter.  If they
    are not present, fill in the default refcenter, and use the
    'SRA Sample' field from the SRA run table"""
    for sampleDescriptorObj in root.iter("SAMPLE_DESCRIPTOR"):
        if (sampleDescriptorObj.get("refcenter") is None
            or re.search("^phs", sampleDescriptorObj.get("refcenter"))):
            sampleDescriptorObj.set("refcenter", refcenter)
        if sampleDescriptorObj.get("refname") is None:
            sampleAccession = sampleDescriptorObj.get("accession")
            if sraRunTable.has_key(sampleAccession):
                refname = sraRunTable[sampleAccession]["Sample Name"]
                sampleDescriptorObj.set("refname", refname)


def checkDirectives(runXml):
    """Check that if the run object has a PROCESSING section, this section
    contains a DIRECTIVES object.  If it does not have one, add an 
    empty DIRECTIVES object"""
    for pp in runXml.iterfind("PROCESSING"):
        if pp.find("DIRECTIVES") is None:
            pp.append(ET.Element("DIRECTIVES"))

def checkLibraryName(experimentXml):
    """If the experiment has an empty library name under the DESIGN object, fill in
    from the experiment object alias"""
    libraryName = experimentXml.find("DESIGN/LIBRARY_DESCRIPTOR/LIBRARY_NAME")
    if libraryName.text is None:
        libraryName.text = experimentXml.get("alias")
 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("objectDir", type=str, 
                        default="perObject",
                        help="Directory with repackaged submissions")
    parser.add_argument('projectAccessionsFile', type=str,
                        help="Project accession mappings")
    parser.add_argument('sraRunTable', type=str,
                        help="SRA Run Table for TARGET from dbGaP")
    parser.add_argument("--srRefcenter", type=str, default="NCI", 
                        help="New value to specify under STUDY_REF")
    parser.add_argument("--tRefcenter", type=str, default="NCI_TARGET", 
                        help="New value to specify under TARGET")
    parser.add_argument("-d", "--debug", type=str, default=False, 
                        help="Display debugging info")
    args = parser.parse_args()

    projectAccessions = common.readDictOfDicts(args.projectAccessionsFile)
    sraRunTable = common.readDictOfDicts(args.sraRunTable, keyColumn=16)
    analysisDir = args.objectDir + "/analysis"
    for analysisObj in os.listdir(analysisDir):
        if args.debug:
            print "working on analysis object", analysisObj 
        analysisPath = analysisDir + "/" + analysisObj
        analysisTree = readXmlFile(analysisPath)
        analysisXml = analysisTree.getroot()
        fixStudyRef(analysisXml, args.srRefcenter, projectAccessions)
        target = analysisXml.find("TARGETS/TARGET")
        target.set("refcenter", args.tRefcenter)
        analysisTree.write(analysisPath)

    runDir = args.objectDir + "/run"
    for runObj in os.listdir(runDir):
        if args.debug:
            print "working on run object", runObj
        runPath = runDir + "/" + runObj
        runTree = readXmlFile(runPath)
        runXml = runTree.getroot()
        checkDirectives(runXml)
        fixExperimentRefRefname(runXml)
        runTree.write(runPath)

    experimentDir = args.objectDir + "/experiment"
    for experimentObj in os.listdir(experimentDir):
        if args.debug:
            print "working on experiment object", experimentObj
        experimentPath = experimentDir + "/" + experimentObj
        experimentTree = readXmlFile(experimentPath)
        experimentXml = experimentTree.getroot() 
        fixStudyRef(experimentXml, args.srRefcenter, projectAccessions)
        fixSampleDescriptor(experimentXml, sraRunTable, args.tRefcenter)
        checkLibraryName(experimentXml)
        experimentTree.write(experimentPath)

        
if __name__ == '__main__':
    main()

    

