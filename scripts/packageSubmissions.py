#!/usr/bin/env python

import argparse
import common
import lxml.etree
import os
import re
from StringIO import StringIO
import subprocess
import sys
import uuid
from xml.dom.ext.reader import Sax2
import xml.dom.ext as ext
import xml.etree.ElementTree as ET
 



def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree.getroot())

def loadExperiment(inputDir, accession):
    """Load an experiment, given its accession and the input path"""
    xmlFilename = inputDir + "/experiment/" + accession + ".xml"
    return(readXmlFile(xmlFilename))

def loadRun(inputDir, accession):
    """Load a run, given its accession and the input path"""
    xmlFilename = inputDir + "/run/" + accession + ".xml"
    return(readXmlFile(xmlFilename))

def loadAnalysis(inputDir, accession):
    """Load an analysis, given its accession and the input path"""
    xmlFilename = inputDir + "/analysis/" + accession + ".xml"
    return(readXmlFile(xmlFilename))

def readMappings(filename, delimiter="\t"):
    """Given a delimited two-column file, return a dictionary where the
    first column is the key and the second is the value"""
    mappings = dict()
    fp = open(filename)
    for line in fp:
        tokens = line.rstrip().split(delimiter)
        assert(len(tokens) == 2)
        mappings[tokens[0]] = tokens[1]
    return mappings

def writeMappings(filename, mappings, delimiter="\t"):
    """Write a mapping dictionary in a two-column file"""
    fp = open(filename, "w")
    for thisKey in mappings.keys():
        thisValue = mappings[thisKey]
        fp.write("%s%s%s\n" % (thisKey, delimiter, thisValue))

def makeKey(object):
    """Given an object, return its xml center/alias key"""
    center = object.get("center_name")
    alias = object.get("alias")
    key = center + "_" + alias
    return(key)

def makeRefKey(object):
    """Given an object, return its xml refcenter/refname key"""
    refcenter = object.get("refcenter")
    refname = object.get("refname")
    key = refcenter + "_" + refname
    return(key)

def reverseDict(oldDict):
    newDict = dict()
    for thisKey in oldDict.keys():
        thisValue = oldDict[thisKey]
        newDict[thisValue] = thisKey
    return(newDict)


def canonicalTextForm(element):
    """Return the canonical text form of an XML document"""
    xmlStringIO = StringIO()
    etree = lxml.etree.fromstring(ET.tostring(element))
    etree.getroottree().write_c14n(xmlStringIO)
    cText = xmlStringIO.getvalue()
    return(cText)
    
def equivalent(element1, element2):
    """Compare two XML objects stored in element trees and return
    a flag indicating if they're equivalent"""
    return(canonicalTextForm(element1) == canonicalTextForm(element2))


def collectExperimentMappings(inputDir):
    """Return a dictionary mapping experiment object key (center, alias)
    to accession"""
    allExperiments = dict()
    for experimentFile in os.listdir(inputDir + "/experiment"):
        experiment = readXmlFile(inputDir + "/experiment/" + experimentFile)
        if not experiment.tag == "EXPERIMENT":
            raise ValueError, "Unexpected file structure in " + experimentFile
        key = makeKey(experiment)
        accession = experiment.get("accession")
        if allExperiments.has_key(key):
            previousObject = loadExperiment(inputDir, allExperiments[key])
            if not equivalent(experiment, previousObject):
                print "Error: different experiments for", key, \
                      " - see", experimentFile, "and", \
                      allExperiments[key]
        allExperiments[key] = accession
    return(allExperiments)



def collectRunMappings(inputDir):
    """Return a dictionary mapping run object key (center, alias)
    to accession"""
    allRuns = dict()
    for runFile in os.listdir(inputDir + "/run"):
        run = readXmlFile(inputDir + "/run/" + runFile)
        if not run.tag == "RUN":
            raise ValueError, "Unexpected file structure in " + runFile
        key = makeKey(run)
        accession = run.get("accession")
        if allRuns.has_key(key):
            previousObject = loadRun(inputDir, allRuns[key])
            if not equivalent(run, previousObject):
                print "Error: different runs for", key, \
                      " - see", runFile, "and", \
                      allRuns[key]
        allRuns[key] = accession
    return(allRuns)


def collectAnalysisMappings(inputDir):
    """Return a dictionary mapping analysis object key (center, alias)
    to accession"""
    allAnalysis = dict()
    for analysisFile in os.listdir(inputDir + "/analysis"):
        analysisFilename = inputDir + "/analysis/" + analysisFile
        analysis = readXmlFile(analysisFilename)
        if not analysis.tag == "ANALYSIS":
            raise ValueError, "Unexpected file structure in " + analysisFile
        key = makeKey(analysis)
        allAnalysis[key] = analysisFilename
    return(allAnalysis)




def copyElement(src):
    """Return a new copy of an element in an element tree"""
    dest = ET.fromstring(ET.tostring(src))
    return(dest)
                         

def makeRunFromTemplate(analysisRunObject, templateRunObject):
    """Given a template run object, and a RUN object from inside an
    analysis object, make a new run object"""
    newRun = copyElement(templateRunObject)
    newRun.set("accession", analysisRunObject.get("accession"))
    newRun.set("alias", analysisRunObject.get("refname"))
    ids = newRun.find("IDENTIFIERS")
    idsToDelete = list()
    for thisId in ids:
        if thisId.tag == "PRIMARY_ID":
            thisId.text = analysisRunObject.get("accession")
        elif thisId.tag == "SECONDARY_ID":
            idsToDelete.append(thisId)
        elif thisId.tag == "SUBMITTER_ID":
            thisId.text = analysisRunObject.get("refname")
        else:
            print "Error: Unexpected tag %s encountered in object %s" \
                  % (thisId.tag, templateRunObject.get("accession"))
    for thisId in idsToDelete:
        ids.remove(thisId)
    return(newRun)
    


def getReferencingObjects(myObj, tagToFind, label, inputDir, debug=True):
    """Given an object, return a list of the objects that it references"""
    myRefSet = list()
    objectsAdded = dict()
    for subObj in myObj.iter(tagToFind):
        objectLabel = subObj.get(label)
        if debug:
            print "Looking for label", label, " found", objectLabel
        objectLabel = re.sub("/", "", objectLabel)
        if not objectsAdded.has_key(objectLabel):
            print "adding object", objectLabel
            pathnameThisObject = "%s/%s.xml" % (inputDir, objectLabel)
            if debug:
                print "pathname", pathnameThisObject
            if os.path.exists(pathnameThisObject):
                objectsAdded[objectLabel] = 1
                newObj = readXmlFile(pathnameThisObject)
                myRefSet.append(newObj)
    return(myRefSet)


def findRunObjs(analysis, label, inputDir, projectAccessions):
    """Given an analysis object, return a run set with the associated
    run objects"""
    print "getting run objects from analysis", analysis.get(label)
    myRunSet = getReferencingObjects(analysis, "RUN", label, inputDir + "/run")
    if len(myRunSet) == 0:
        print "Error: can't find any run objects for", \
            analysis.get("accession")
    return(myRunSet)


#def findSecondaryRunObjs(runSet, inputDir, projectAccessions, 
#                         label="accession", debug=False):
#    """Given a set of run objects, add any secondary run objects to the set"""
#    runObjectsInSet = dict()
#    if debug:
#        print "getting secondary run objects from run set"
#    # First, make a list of the objects already in the set
#    for run in runSet:
#        runObjectsInSet[run.get(label)] = 1
#    # Now, for the objects in the set, look for any SECONDARY_IDs
#    # with the SRR prefix
#    for run in runSet:
#        for secondaryId in run.iter("SECONDARY_ID"):
#            secondaryAccession = secondaryId.text
#            if re.search("^SRR", secondaryAccession):
#                if not runObjectsInSet.has_key(secondaryAccession):
#                    if debug:
#                        print "Found new run obj", secondaryAccession
#                    pathnameThisRun = "%s/run/%s.xml" % (inputDir, 
#                                                         secondaryAccession)
#                    if os.path.exists(pathnameThisRun):
#                        if debug:
#                            print "Adding", secondaryAccession
#                        newRun = readXmlFile(pathnameThisRun)
#                        runSet.append(newRun)
#                        runObjectsInSet[secondaryAccession] = 1
#    return(runSet)




def findExperimentObjs(runSet, label, inputDir):
    """Given a set of run objects, return the set of experiment objects
    that they reference"""
    myExpSet = list()
    objectsAdded = dict()
    print "Getting experiment from run"
    for run in runSet:
        expSetThisRun = getReferencingObjects(run, "EXPERIMENT_REF", label,
                                              inputDir + "/experiment")
        for item in expSetThisRun:
            key = makeKey(item)
            if not objectsAdded.has_key(key):
                myExpSet.append(item)
                objectsAdded[key] = 1
    if len(myExpSet) == 0:
        print "Error: can't find any experiment objects"
    return(myExpSet)

def setUuid(myUuid, myAnalysisObj):
    """Set the IDENTIFIERS/UUID field to indicate this UUID"""
    myIdentifierObj = myAnalysisObj.find("IDENTIFIERS")
    if myIdentifierObj is not None:
        myUuidObj = myIdentifierObj.find("UUID")
        if myUuidObj is not None:
            myUuidObj.text = myUuid
        else:
            myUuidObj = ET.SubElement(myIdentifierObj, "UUID")
            myUuidObj.text = myUuid
    else:
        myIdentifierObj = ET.Element("IDENTIFIERS")
        myUuidObj = ET.SubElement(myIdentifierObj, "UUID")
        myUuidObj.text = myUuid
        myAnalysisObj.insert(0, myIdentifierObj)
        


def prettify(etNode):
    """Given an ElementTree element, return a prettified element with a clean
    indentation structure"""
    reader = Sax2.Reader()
    docNode = reader.fromString(canonicalTextForm(etNode))
    tmpStream = StringIO()
    ext.PrettyPrint(docNode, stream=tmpStream)
    return tmpStream.getvalue()


def writePrettyTree(myTree, outputFile):
    """Given an element tree and a target file, write a pretty version of the
    tree into the target file"""
    fp = open(outputFile, "w")
    fp.write(prettify(myTree))


def saveSet(mySet, name, outputDir):
    """Save the indicated set of objects under the indicated name, provided
    that the set contains at least one object"""
    if len(mySet) > 0:
        nameThisSet = (name + "_set").upper()
        elementSet = ET.Element(nameThisSet)
        for myObj in mySet:
            elementSet.append(myObj)
        outputFile = "%s/%s.xml" % (outputDir, name)
        writePrettyTree(elementSet, outputFile)
    


def projectInList(obj, referenceSet):
    """See if the project for this sample is in the list of expected projects."""
    for studyRefObj in obj.iter("STUDY_REF"):
        if ("accession" in studyRefObj.keys()):
            projectAccession = studyRefObj.get("accession")
            if (projectAccession in referenceSet):
                return True
    return False
    

def cleanUpStudyRef(root, studyAccessionMapping,
                    studyrefcenter="National Cancer Institute", debug=False):
    """Ensure that the STUDY_REF object contains refname and refcenter
    attributes.  If they are missing, add them"""
    for studyRefObj in root.iter("STUDY_REF"):
        if (studyRefObj.get("refcenter") is None 
            or studyRefObj.get("refcenter") == "dbgap"):
            studyRefObj.set("refcenter", studyrefcenter)
        if studyRefObj.get("refname") is None:
            projectAccession = studyRefObj.get("accession")
            if debug:
                print "project accession", projectAccession, "refname", \
                    studyAccessionMapping[projectAccession]["dbGaP_Accession"]
            if studyAccessionMapping.has_key(projectAccession):
                refname = studyAccessionMapping[projectAccession]["dbGaP_Accession"]
                studyRefObj.set("refname", refname) 


def cleanUpTarget(root, targetrefcenter="NCI_TARGET"):
    for targetObj in root.iter("TARGET"):
        if targetObj.get("refcenter") is None \
                or re.search("^phs", targetObj.get("refcenter")):
            targetObj.set("refcenter", targetrefcenter)



def cleanUpSampleDescriptor(root, runTable, refcenter="NCI_TARGET"):
    """Ensure that the sample descriptor has a refname and refcenter.  If they
    are not present, fill in the default refcenter, and use the
    barcode field as refname.  This is in the run table as Name.
    """
    for sampleDescriptorObj in root.iter("SAMPLE_DESCRIPTOR"):
        if (sampleDescriptorObj.get("refcenter") is None
            or re.search("^phs", sampleDescriptorObj.get("refcenter"))):
            sampleDescriptorObj.set("refcenter", refcenter)
        if sampleDescriptorObj.get("refname") is None:
            sampleAccession = sampleDescriptorObj.get("accession")
            if runTable.has_key(sampleAccession):
                refname = runTable[sampleAccession]["Sample Name"]
                sampleDescriptorObj.set("refname", refname)


def cleanupObjectSet(myObjSet, projectAccessions, runTable):
    """Do minor cleanup if the xml object is broken in one of a few 
    predictable and easily-fixed ways"""
    for myObj in myObjSet:
        cleanUpStudyRef(myObj, projectAccessions)
        cleanUpTarget(myObj)
        cleanUpSampleDescriptor(myObj, runTable)

def cleanupSubmissionSet(myExpSet, myRunSet, myAnalysisSet, 
                         projectAccessions, runTable):
    """Do minor cleanup if the submission is broken in one of a few 
    predictable and easily-fixed ways"""
    for mySubmissionSet in (myExpSet, myRunSet, myAnalysisSet):
        cleanupObjectSet(mySubmissionSet, projectAccessions, runTable)


def saveSubmission(myExpSet, myRunSet, myAnalysisSet, myUuid, outputDir):
    """Write out the submission under the indicated output directory"""
    center = myAnalysisSet[0].get("center_name")
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    targetDir = outputDir + "/" + center
    if not os.path.exists(targetDir):
        os.mkdir(targetDir)
    outputDirThisSubmission = targetDir + "/" + myUuid
    if not os.path.exists(outputDirThisSubmission):
        os.mkdir(outputDirThisSubmission)
    if len(myExpSet) > 1:
        print "Error: %d experiments under uuid %s" % (len(myExpSet), uuid)
    saveSet(myExpSet, "experiment", outputDirThisSubmission)
    saveSet(myRunSet, "run", outputDirThisSubmission)
    saveSet(myAnalysisSet, "analysis", outputDirThisSubmission)
    print "Saved to directory", outputDirThisSubmission

    
    


def saveSubmissionThisAnalysisObj(inputDir, myAnalysisFilename, label,
                                  projectAccessions,
                                  runTableBySample,
                                  outputDir, doCleanup=True):
    """Package up and save the submission for this analysis obj"""
    myAnalysisObj = readXmlFile(myAnalysisFilename)
    """Before proceeding, make sure this is one of the projects we're expecting"""
    if projectInList(myAnalysisObj, projectAccessions.keys()):
        print "working on analysis", myAnalysisFilename

        # To get a deterministic UUID, generate a UUID from the
        # analysis accession (or pseudo-accession, in the case of
        # analysis files that were generated without accessions.  This
        # entails calling uuid5, accessing the urn field to get the
        # uuid in the format 'urn:uuid:<uuid>', and extracting the
        # UUID from the end.
        analysisAccession = myAnalysisFilename.split("/")[-1]
        analysisAccession = re.sub(".xml$", "", analysisAccession)
        myUuid = uuid.uuid5(uuid.NAMESPACE_URL, 
                            analysisAccession).urn.split(":")[-1]
        setUuid(myUuid, myAnalysisObj)
        myRunSet = findRunObjs(myAnalysisObj, label, inputDir, 
                               projectAccessions)
#        myRunSet = findSecondaryRunObjs(myRunSet, inputDir, projectAccessions)
        myExpSet = findExperimentObjs(myRunSet, label, inputDir)
        myAnalysisSet = []
        myAnalysisSet.append(myAnalysisObj)
        if doCleanup:
            cleanupSubmissionSet(myExpSet, myRunSet, myAnalysisSet, 
                                 projectAccessions, runTableBySample)
        saveSubmission(myExpSet, myRunSet, myAnalysisSet, myUuid, outputDir)
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("perObjectDir", type=str, 
                        help="Input directory for mal-packaged metadata")
    parser.add_argument('projectAccessionsFile', type=str,
                        help="Project accession mappings")
    parser.add_argument('manifestFile', type=str,
                        help="Project manifest from dbGaP")
    parser.add_argument("outputDir", type=str, 
                    help="Output directory for packaged metadata")
    parser.add_argument("--label", type=str, default="accession",
                        help="""Label by which the objects are labeled 
                                under the perObject dir""")
    args = parser.parse_args()

    analysisKeyToFilename = collectAnalysisMappings(args.perObjectDir)

    projectAccessions = common.readDictOfDicts(args.projectAccessionsFile)
    runTableBySample = common.readDictOfDicts(args.manifestFile, keyColumn=16)
    for myAnalysisFilename in analysisKeyToFilename.values():
        saveSubmissionThisAnalysisObj(args.perObjectDir, myAnalysisFilename,
                                      args.label, projectAccessions, 
                                      runTableBySample,  args.outputDir)
    

if __name__ == '__main__':
    main( )


