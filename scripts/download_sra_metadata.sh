#!/bin/bash

#get xml from SRA
cat $1 | perl -ne 'chomp; print "curl \"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=SRA&term=$_\" -o $_.esearch\n";' > $1.esearches.sh

chmod a+x $1.esearches.sh
/bin/bash $1.esearches.sh

rm -- -.esearch

ls *.esearch | perl -ne 'chomp; $f1=$_; @e=`egrep -e "<Id>" $f1`; chomp(@e); for $e (@e) { $e=~/<Id>(\d+)<\/Id>/; $id=$1; print "$f1\t$id\n";}' >  $1.all_ids

cat $1.all_ids | perl -ne 'chomp; ($n,$i)=split(/\t/,$_); print "curl \"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=SRA&id=$i\" -o $n.$i.sra.md.xml >> $i.mdgrab\n";' > $1.all_ids.mdgrab.sh

chmod a+x $1.all_ids.mdgrab.sh
/bin/bash $1.all_ids.mdgrab.sh

#now pull out the specific fields you want, varies by project/context
#ls *.xml | perl -ne 'chomp; $f=$_; open(IN,"<$f"); $g="unknown"; $p="unknown"; while($line = <IN>) { chomp($f); if($line=~/>male</i) { $g="male";} elsif($line =~ />female</i) { $g="female";} if($line =~ /published="([^"]+)"/i) { $p=$1; } } print "$f\t$g\t$p\n";' | sort -k3,3 > all_ids.gender_published_dates
rm *.esearch
rm *.mdgrab
rm *.sh
