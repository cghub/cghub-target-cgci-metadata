#!/usr/bin/env python

import argparse
import csv
import os
import re
import string
import subprocess
import sys
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)

def warn(message):
    sys.stderr.write(message + "\n")


def buildRunToExperiment(pathname):
    """Given a set of accessioned but lower-quality run objects, return
    a dictionary indicating which experiment object corresponds to each run"""
    runToExperiment = dict()
    for runXmlFile in os.listdir(pathname):
        runObj = readXmlFile(pathname + "/" + runXmlFile).getroot()
        runAccession = runObj.get("accession")
        assert(runAccession is not None)
        experimentAccession = runObj.find("EXPERIMENT_REF").get("accession")
        runToExperiment[runAccession] = experimentAccession
    return(runToExperiment)



def keyDictOfDicts(filename, keyList, delimiter="\t", listAll=False):
    """Given a delimited file, return a dict of dicts, where the
    key to the outer dict is assembled from the list of indicated columns and
    the keys to the inner dicts are the header row columns.  In other
    words, return a dict for which the key is the contents of the first
    column, and the value for each of those keys is a dict for which the
    keys are the column labels"""
    dictOfDicts = dict()
    dd = csv.DictReader(open(filename), delimiter=delimiter)
    for row in dd:
        key = ""
        delimiter = ""
        for item in keyList:
            key = key + delimiter + string.upper(row[item])
            delimiter = "_"
        newKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", key))
        newDictDict = dict()
        for thisSubKey in row.keys():
            thisValue = row[thisSubKey]
            thisSubKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", thisSubKey))
            if thisValue is not None:
                thisValue = re.sub("(\s)*$", "", re.sub("^(\s)*", "",
                                                        thisValue))
            newDictDict[thisSubKey] = thisValue
        if not listAll:
            dictOfDicts[newKey] = newDictDict
        elif dictOfDicts.has_key(newKey):
            dictOfDicts[newKey].append(newDictDict)
        else:
            dictOfDicts[newKey] = list()
            dictOfDicts[newKey].append(newDictDict)
    return dictOfDicts




def alreadyAccessioned(thisSubmissionDir):
    """Determine if the run object already has an accession"""
    thisRunXml = thisSubmissionDir + "/run.xml"
    if os.path.exists(thisRunXml):
        thisRunObj = readXmlFile(thisRunXml).getroot().find("RUN")
        return (thisRunObj.get("accession") is not None)
    else:
        return False



def labelThisSubmission(thisSubmissionDir, sraRunTable, runToExperiment):
    experimentFile = thisSubmissionDir + "/experiment.xml"
    if not os.path.exists(experimentFile):
        warn(("Warning: no experiment xml in %s" % (thisSubmissionDir)))
    else:
        experimentXml = readXmlFile(experimentFile).getroot().find("EXPERIMENT")
        sampleDescriptorObj = experimentXml.find("DESIGN/SAMPLE_DESCRIPTOR")
        barcode = sampleDescriptorObj.get("refname")
        libraryDescriptorObj = experimentXml.find("DESIGN/LIBRARY_DESCRIPTOR")
        library = libraryDescriptorObj.find("LIBRARY_NAME").text
        key = "%s_%s" % (string.upper(barcode), string.upper(library))
        key = re.sub("(\s)*", "", key)
        if not sraRunTable.has_key(key):
            warn(("Error: key %s not found for dir %s" % (key, 
                                                          thisSubmissionDir)))
        else:
            sraRunTableEntry = sraRunTable[key]
            runAccession = sraRunTableEntry["Run"]
            experimentAccession = runToExperiment[runAccession]
            runFile = thisSubmissionDir + "/run.xml"
            runTree = readXmlFile(runFile)
            runObj = runTree.getroot().find("RUN")
            runObj.set("accession", runAccession)
            runObj.find("EXPERIMENT_REF").set("accession", experimentAccession)
            runTree.write(runFile)
            experimentFile = thisSubmissionDir + "/experiment.xml"
            experimentTree = readXmlFile(experimentFile)
            experimentObj = experimentTree.getroot().find("EXPERIMENT")
            experimentObj.set("accession", experimentAccession)
            experimentTree.write(experimentFile)
            print "just labeled %s with %s" % (thisSubmissionDir, runAccession)
    
    


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--submissionDir", type=str,
                        default="repackaged/TARGET/BCCAGSC",
                        help="Directory with submissions which lack run accessions")
    parser.add_argument("-t", "--table", type=str,
                        default="../../inputs/SraRunTable.TARGET.txt",
                        help="SRA Run table")
    parser.add_argument("-r", "--runObjDir", type=str,
                        default="perObject-NCBI/run",
                        help="Lower-quality but accessioned run objects")
    parser.add_argument("-d", "--debug", type=int, default=False)
    args = parser.parse_args()

    runToExperiment = buildRunToExperiment(args.runObjDir)
    sraRunTable = keyDictOfDicts(args.table, 
                                 ("submitted_sample_id", "Library Name"),
                                 delimiter="\t")
    for submission in os.listdir(args.submissionDir):
        thisSubmissionDir = args.submissionDir + "/" + submission
        if not alreadyAccessioned(thisSubmissionDir):            
            labelThisSubmission(thisSubmissionDir, sraRunTable, 
                                runToExperiment)

        
if __name__ == '__main__':
    main()

    

